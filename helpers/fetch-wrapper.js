export const fetchWrapper = {
  post,
};

function post(url, body) {
  const bodyToSend = JSON.stringify(body);
  const requestOptions = {
    method: "POST",
    headers: {
      Accept: "*/*",
      "Content-Type": "application/json",
      "Content-Length": bodyToSend.length,
      Host: "tido-payment.herokuapp.com",
    },
    body: bodyToSend,
  };
  return fetch(url, requestOptions);
}
