import { signup, login } from "../../services";
import { userActionTypes } from "../actionTypes";

export const signupAction = (user) => {
  return async (dispatch) => {
    try {
      const res = await signup(user);
      const result = await res.json();
      dispatch({
        type: userActionTypes.SIGN_UP_SUCCESS,
      });
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  };
};

export const loginAction = (email, password) => {
  return async (dispatch) => {
    try {
      const response = await login(email, password);
      const userData = await response.json();
      localStorage.setItem("user", JSON.stringify(userData));
      dispatch({
        type: userActionTypes.LOGIN_SUCCESS,
        userData,
      });
      return true;
    } catch (error) {
      return false;
    }
  };
};

export const logoutAction = () => {
  return (dispatch) => {
    localStorage.removeItem("user");
    dispatch({
      type: userActionTypes.LOGOUT_SUCCESS,
    });
    return true;
  };
};
