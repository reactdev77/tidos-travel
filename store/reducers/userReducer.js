import { userActionTypes } from "../actionTypes";

const loggedInUser =
  typeof window !== "undefined" && JSON.parse(localStorage.getItem("user"));
const initialState = {
  isSignedUp: false,
  isLoggedIn: loggedInUser ? true : false,
  userInfor: loggedInUser,
};

export const user = (state = initialState, action) => {
  switch (action.type) {
    case userActionTypes.SIGN_UP_SUCCESS:
      return {
        ...state,
        isSignedUp: true,
      };
    case userActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        userInfor: { ...action.userData },
      };
    case userActionTypes.LOGOUT_SUCCESS:
      return {
        ...state,
        isSignedUp: false,
        isLoggedIn: false,
        userInfor: null,
      };
    default:
      return state;
  }
};
