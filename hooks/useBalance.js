import { useEffect, useState } from "react";
import Web3 from "web3";
import { TokenService } from "../services";
import { useSelector } from "react-redux";

const useBalance = (address, chainId) => {
  const user = useSelector((state) => state.user);
  const [balance, setBalance] = useState(0);
  const [tidosBalance, setTidosBalance] = useState(0);

  useEffect(() => {
    //Get balance from both external wallet and built-in wallet
    const getBalance = async () => {
      try {
        const testBSC_URL = "https://data-seed-prebsc-1-s1.binance.org:8545";
        const mainBSC_URL = "https://bsc-dataseed.binance.org:443";
        const web3 = new Web3(mainBSC_URL);
        const { tokenAddress, ABI } = TokenService;
        const tokenContractInst = new web3.eth.Contract(ABI, tokenAddress);
        const tidosBalance = await tokenContractInst.methods
          .balanceOf(user.userInfor?.address)
          .call();
        setTidosBalance(web3.utils.fromWei(tidosBalance, "ether"));
        if (chainId === 56) {
          const balance = await tokenContractInst.methods
            .balanceOf(address)
            .call();
          setBalance(web3.utils.fromWei(balance, "ether"));
        }
      } catch (error) {
        console.log(error);
      }
    };
    getBalance();
  }, [address, chainId, user.userInfor?.address]);

  return {
    balance,
    tidosBalance,
  };
};

export default useBalance;
