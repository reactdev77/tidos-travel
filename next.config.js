// /** @type {import('next').NextConfig} */
// const nextConfig = {
//   reactStrictMode: true,
// }

// module.exports = nextConfig

const webpack = require("webpack");

module.exports = {
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    config.plugins.push(
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery",
      })
    );

    // Important: return the modified config
    return config;
  },
};

module.exports = {
  async redirects() {
    return [
      {
        source: "/hotels",
        destination: "/",
        permanent: true,
      },
      {
        source: "/hotel",
        destination: "/",
        permanent: true,
      },
      {
        source: "/flight",
        destination: "/flights",
        permanent: true,
      },
    ];
  },
};
