import Router from "next/router";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import { logoutAction } from "../../store/actions";
import Layout from "../../components/layout/Layout";
import MainSection from "../../components/user/MainSection";

const Profile = () => {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logoutAction());
  };

  useEffect(() => {
    if (!user?.isLoggedIn) Router.push("/");
  }, [user?.isLoggedIn]);

  return (
    <>
      {user?.isLoggedIn ? (
        <Layout>
          <MainSection name={user?.userInfor?.name}>
            <div className="col-xs-12 col-sm-10 col-md-10 dashboard-content user-profile">
              <h2 className="dash-content-title">My Account</h2>
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h4>Account Details</h4>
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-12 col-md-12  user-detail">
                      <ul className="list-unstyled">
                        <li>
                          <span>Name:</span> {user.userInfor?.name}
                        </li>
                        <li>
                          <span>Email:</span> {user.userInfor?.email}
                        </li>
                      </ul>
                      <button
                        className="btn"
                        data-toggle="modal"
                        data-target="#edit-profile"
                        onClick={handleLogout}
                      >
                        Log out
                      </button>
                    </div>

                    <div className="col-sm-12 user-desc">
                      <h4>About You</h4>
                      <p>
                        Vestibulum tristique, justo eu sollicitudin sagittis,
                        metus dolor eleifend urna, quis scelerisque purus quam
                        nec ligula. Suspendisse iaculis odio odio, ac vehicula
                        nisi faucibus eu. Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit. Suspendisse posuere semper
                        sem ac aliquet. Duis vel bibendum tellus, eu hendrerit
                        sapien. Proin fringilla, enim vel lobortis viverra,
                        augue orci fringilla diam, sed cursus elit mi vel lacus.
                        Nulla facilisi. Fusce sagittis, magna non vehicula
                        gravida, ante arcu pulvinar arcu, aliquet luctus arcu
                        purus sit amet sem. Mauris blandit odio sed nisi
                        porttitor egestas. Mauris in quam interdum purus
                        vehicula rutrum quis in sem. Integer interdum lectus at
                        nulla dictum luctus. Sed risus felis, posuere id
                        condimentum non, egestas pulvinar enim. Praesent pretium
                        risus eget nisi ullamcorper fermentum. Duis lacinia nisi
                        ac rhoncus vestibulum.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </MainSection>
        </Layout>
      ) : (
        <></>
      )}
    </>
  );
};

export default Profile;
