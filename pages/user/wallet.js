import NumberFormat from "react-number-format";
import { useSelector } from "react-redux";
import { useEffect } from "react";

import Layout from "../../components/layout/Layout";
import MainSection from "../../components/user/MainSection";
import { useWeb3Context } from "../../web3/providers/Web3ContextProvider";
import useBalance from "../../hooks/useBalance";

const Wallet = () => {
  const user = useSelector((state) => state.user);
  const { address, chainId, connect } = useWeb3Context();
  const { balance, tidosBalance } = useBalance(address, chainId);

  useEffect(() => {
    if (!user?.isLoggedIn) Router.push("/");
  }, [user?.isLoggedIn]);

  return (
    <>
      {user?.isLoggedIn ? (
        <Layout>
          <MainSection name={user?.userInfor?.name}>
            <div className="col-xs-12 col-sm-10 col-md-10 dashboard-content user-profile">
              <h2 className="dash-content-title">My Wallet</h2>
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h4>External Wallet Details</h4>
                </div>
                <div className="panel-body">
                  <div className="row">
                    {address && chainId !== 56 && (
                      <div style={{ marginBottom: 30 }}>
                        <div className="alert alert-danger" role="alert">
                          We accept only Tidos token on Binance Smart Chain,
                          please make sure you connected to BSC chain.
                        </div>
                        <a
                          onClick={connect}
                          style={{
                            cursor: "pointer",
                            textDecorationLine: "none",
                          }}
                        >
                          Switch to Binance Smart Chain
                        </a>
                      </div>
                    )}
                    <div className="col-sm-12 col-md-12  user-detail">
                      <ul className="list-unstyled">
                        <li>
                          <span>Tidos token balance: </span>
                          <span>
                            <NumberFormat
                              value={address && chainId === 56 ? balance : 0}
                              displayType={"text"}
                              decimalScale={3}
                              thousandSeparator={true}
                            />
                          </span>
                        </li>
                        <li>
                          <span>Address: </span>
                          <span style={{ wordBreak: "break-word" }}>
                            {address ? address : ""}
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="panel panel-default" style={{ marginTop: 30 }}>
                <div className="panel-heading">
                  <h4>Built-in Wallet Details</h4>
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-12 col-md-12  user-detail">
                      <ul className="list-unstyled">
                        <li>
                          <span>Tidos token balance: </span>
                          <span>
                            <NumberFormat
                              value={tidosBalance}
                              displayType={"text"}
                              decimalScale={3}
                              thousandSeparator={true}
                            />
                          </span>
                        </li>
                        <li>
                          <span>Deposit address: </span>
                          <span style={{ wordBreak: "break-word" }}>
                            {user.userInfor?.address}
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </MainSection>
        </Layout>
      ) : (
        <></>
      )}
    </>
  );
};

export default Wallet;
