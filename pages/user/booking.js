import Router from "next/router";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import Layout from "../../components/layout/Layout";
import MainSection from "../../components/user/MainSection";

const Booking = () => {
  const user = useSelector((state) => state.user);

  useEffect(() => {
    if (!user?.isLoggedIn) Router.push("/");
  }, [user?.isLoggedIn]);

  return (
    <>
      {user?.isLoggedIn ? (
        <Layout>
          <MainSection name={user?.userInfor?.name}>
            <div className="col-xs-12 col-sm-10 col-md-10 dashboard-content booking-trips">
              <h2 className="dash-content-title">Trips You have Booked!</h2>
              <div className="dashboard-listing booking-listing">
                <div className="dash-listing-heading">
                  <div className="custom-radio">
                    <input type="radio" id="radio01" name="radio" checked />
                    <label htmlFor="radio01">
                      <span></span>All Types
                    </label>
                  </div>

                  <div className="custom-radio">
                    <input type="radio" id="radio02" name="radio" />
                    <label htmlFor="radio02">
                      <span></span>Hotels
                    </label>
                  </div>

                  <div className="custom-radio">
                    <input type="radio" id="radio03" name="radio" />
                    <label htmlFor="radio03">
                      <span></span>Flights
                    </label>
                  </div>
                </div>

                <div className="table-responsive">
                  <table className="table table-hover">
                    <tbody>
                      <tr>
                        <td className="dash-list-icon booking-list-date">
                          <div className="b-date">
                            <h3>18</h3>
                            <p>October</p>
                          </div>
                        </td>
                        <td className="dash-list-text booking-list-detail">
                          <h3>{`Tom's Restaurant`}</h3>
                          <ul className="list-unstyled booking-info">
                            <li>
                              <span>Booking Date:</span> 26.12.2017 at 03:20 pm
                            </li>
                            <li>
                              <span>Booking Details:</span> 3 to 6 People
                            </li>
                            <li>
                              <span>Client:</span> Lisa Smith
                              <span className="line">|</span>
                              lisasmith@youremail.com
                              <span className="line">|</span>125 254 2578
                            </li>
                          </ul>
                          <button className="btn btn-orange">Message</button>
                        </td>
                        <td className="dash-list-btn">
                          <button className="btn btn-orange">Cancel</button>
                          <button className="btn">Approve</button>
                        </td>
                      </tr>

                      <tr>
                        <td className="dash-list-icon booking-list-date">
                          <div className="b-date">
                            <h3>18</h3>
                            <p>October</p>
                          </div>
                        </td>
                        <td className="dash-list-text booking-list-detail">
                          <h3>{`Tom's Restaurant`}</h3>
                          <ul className="list-unstyled booking-info">
                            <li>
                              <span>Booking Date:</span> 26.12.2017 at 03:20 pm
                            </li>
                            <li>
                              <span>Booking Details:</span> 3 to 6 People
                            </li>
                            <li>
                              <span>Client:</span> Lisa Smith
                              <span className="line">|</span>
                              lisasmith@youremail.com
                              <span className="line">|</span>125 254 2578
                            </li>
                          </ul>
                          <button className="btn btn-orange">Message</button>
                        </td>
                        <td className="dash-list-btn">
                          <button className="btn btn-orange">Cancel</button>
                          <button className="btn">Approve</button>
                        </td>
                      </tr>

                      <tr>
                        <td className="dash-list-icon booking-list-date">
                          <div className="b-date">
                            <h3>18</h3>
                            <p>October</p>
                          </div>
                        </td>
                        <td className="dash-list-text booking-list-detail">
                          <h3>{`Tom's Restaurant`}</h3>
                          <ul className="list-unstyled booking-info">
                            <li>
                              <span>Booking Date:</span> 26.12.2017 at 03:20 pm
                            </li>
                            <li>
                              <span>Booking Details:</span> 3 to 6 People
                            </li>
                            <li>
                              <span>Client:</span> Lisa Smith
                              <span className="line">|</span>
                              lisasmith@youremail.com
                              <span className="line">|</span>125 254 2578
                            </li>
                          </ul>
                          <button className="btn btn-orange">Message</button>
                        </td>
                        <td className="dash-list-btn">
                          <button className="btn btn-orange">Cancel</button>
                          <button className="btn">Approve</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </MainSection>
        </Layout>
      ) : (
        <></>
      )}
    </>
  );
};

export default Booking;
