import Router from "next/router";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Layout from "../../components/layout/Layout";
import MainSection from "../../components/user/MainSection";

const Dashboard = () => {
  const user = useSelector((state) => state.user);

  useEffect(() => {
    if (!user?.isLoggedIn) Router.push("/");
  }, [user?.isLoggedIn]);

  return (
    <>
      {user?.isLoggedIn ? (
        <Layout>
          <MainSection name={user?.userInfor?.name}>
            <div className="col-xs-12 col-sm-10 col-md-10 dashboard-content">
              <h2 className="dash-content-title">Total Traveled</h2>
              <div className="row info-stat">
                <div className="col-sm-6 col-md-3">
                  <div className="stat-block">
                    <span>
                      <i className="fa fa-tachometer"></i>
                    </span>
                    <h3>1548</h3>
                    <p>Miles</p>
                  </div>
                </div>

                <div className="col-sm-6 col-md-3">
                  <div className="stat-block">
                    <span>
                      <i className="fa fa-globe"></i>
                    </span>
                    <h3>12%</h3>
                    <p>World</p>
                  </div>
                </div>

                <div className="col-sm-6 col-md-3">
                  <div className="stat-block">
                    <span>
                      <i className="fa fa-building"></i>
                    </span>
                    <h3>312</h3>
                    <p>Cities</p>
                  </div>
                </div>

                <div className="col-sm-6 col-md-3">
                  <div className="stat-block">
                    <span>
                      <i className="fa fa-paper-plane"></i>
                    </span>
                    <h3>102</h3>
                    <p>Trips</p>
                  </div>
                </div>
              </div>

              <div className="dashboard-listing recent-activity">
                <h3 className="dash-listing-heading">Recent Activites</h3>
                <div className="table-responsive">
                  <table className="table table-hover">
                    <tbody>
                      <tr>
                        <td className="dash-list-icon recent-ac-icon">
                          <i className="fa fa-bars"></i>
                        </td>
                        <td className="dash-list-text recent-ac-text">
                          Your listing <span>The Star Travel</span> has been
                          approved!
                        </td>
                        <td className="dash-list-btn del-field">
                          <button className="btn">X</button>
                        </td>
                      </tr>

                      <tr>
                        <td className="dash-list-icon recent-ac-icon">
                          <i className="fa fa-star"></i>
                        </td>
                        <td className="dash-list-text recent-ac-text">
                          Kathy Brown left a review on{" "}
                          <span>The Star Travel</span>
                        </td>
                        <td className="dash-list-btn del-field">
                          <button className="btn">X</button>
                        </td>
                      </tr>

                      <tr>
                        <td className="dash-list-icon recent-ac-icon">
                          <i className="fa fa-bookmark"></i>
                        </td>
                        <td className="dash-list-text recent-ac-text">
                          Someone bookmarked your Norma <span>Spa Center</span>{" "}
                          listing!
                        </td>
                        <td className="dash-list-btn del-field">
                          <button className="btn">X</button>
                        </td>
                      </tr>

                      <tr>
                        <td className="dash-list-icon recent-ac-icon">
                          <i className="fa fa-star"></i>
                        </td>
                        <td className="dash-list-text recent-ac-text">
                          Kathy Brown left a review on{" "}
                          <span>Auto Repair Shop</span>
                        </td>
                        <td className="dash-list-btn del-field">
                          <button className="btn">X</button>
                        </td>
                      </tr>

                      <tr>
                        <td className="dash-list-icon recent-ac-icon">
                          <i className="fa fa-bookmark"></i>
                        </td>
                        <td className="dash-list-text recent-ac-text">
                          Someone bookmarked your{" "}
                          <span>The Star Apartment</span> listing!
                        </td>
                        <td className="dash-list-btn del-field">
                          <button className="btn">X</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div className="dashboard-listing invoices">
                <h3 className="dash-listing-heading">Invoices</h3>
                <div className="table-responsive">
                  <table className="table table-hover">
                    <tbody>
                      <tr>
                        <td className="dash-list-icon invoice-icon">
                          <i className="fa fa-bars"></i>
                        </td>
                        <td className="dash-list-text invoice-text">
                          <h4 className="invoice-title">Professional Plan</h4>
                          <ul className="list-unstyled list-inline invoice-info">
                            <li className="invoice-status red">Unpaid</li>
                            <li className="invoice-order"> Order: #00214</li>
                            <li className="invoice-date"> Date: 25/08/2017</li>
                          </ul>
                        </td>
                        <td className="dash-list-btn">
                          <button className="btn btn-orange">
                            View Invoice
                          </button>
                        </td>
                      </tr>

                      <tr>
                        <td className="dash-list-icon invoice-icon">
                          <i className="fa fa-bars"></i>
                        </td>
                        <td className="dash-list-text invoice-text">
                          <h4>Extended Plan</h4>
                          <ul className="list-unstyled list-inline invoice-info">
                            <li className="invoice-status green">Paid</li>
                            <li className="invoice-order"> Order: #00214</li>
                            <li className="invoice-date"> Date: 25/08/2017</li>
                          </ul>
                        </td>
                        <td className="dash-list-btn">
                          <button className="btn btn-orange">
                            View Invoice
                          </button>
                        </td>
                      </tr>

                      <tr>
                        <td className="dash-list-icon invoice-icon">
                          <i className="fa fa-bars"></i>
                        </td>
                        <td className="dash-list-text invoice-text">
                          <h4>Extended Plan</h4>
                          <ul className="list-unstyled list-inline invoice-info">
                            <li className="invoice-status red">Unpaid</li>
                            <li className="invoice-order"> Order: #00214</li>
                            <li className="invoice-date"> Date: 25/08/2017</li>
                          </ul>
                        </td>
                        <td className="dash-list-btn">
                          <button className="btn btn-orange">
                            View Invoice
                          </button>
                        </td>
                      </tr>

                      <tr>
                        <td className="dash-list-icon invoice-icon">
                          <i className="fa fa-bars"></i>
                        </td>
                        <td className="dash-list-text invoice-text">
                          <h4>Basic Plan</h4>
                          <ul className="list-unstyled list-inline invoice-info">
                            <li className="invoice-status red">Unpaid</li>
                            <li className="invoice-order"> Order: #00214</li>
                            <li className="invoice-date"> Date: 25/08/2017</li>
                          </ul>
                        </td>
                        <td className="dash-list-btn">
                          <button className="btn btn-orange">
                            View Invoice
                          </button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </MainSection>
        </Layout>
      ) : (
        <></>
      )}
    </>
  );
};

export default Dashboard;
