import React from "react";
import Router from "next/router";
import { wrapper, store } from "../store/store";
import { Provider } from "react-redux";
import { ChakraProvider } from "@chakra-ui/react";
import Web3ContextProvider from "../web3/providers/Web3ContextProvider";

import "../styles/bootstrap.min.css";
import "../styles/font-awesome.min.css";
import "../styles/globals.css";
import "../styles/orange.css";
import "../styles/responsive.css";
import "../styles/datepicker.css";

const startLoading = () => {
  document.querySelector(".loader").style.visibility = "visible";
};

const stopLoading = () => {
  document.querySelector(".loader").style.visibility = "hidden";
};

Router.events.on("routeStart", startLoading);
Router.events.on("routeChangeStart", startLoading);
Router.events.on("routeChangeComplete", stopLoading);

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider>
      <Web3ContextProvider>
        <Provider store={store}>
          <div className="loader"></div>
          <Component {...pageProps} />
        </Provider>
      </Web3ContextProvider>
    </ChakraProvider>
  );
}

export default wrapper.withRedux(MyApp);
