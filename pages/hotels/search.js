import Link from "next/link";
import { useState } from "react";
import { useSelector } from "react-redux";
import DatePicker from "react-datepicker";

import Layout from "../../components/layout/Layout";
import SearchResult from "../../components/hotel/search";

const HotelsResult = () => {
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();

  return (
    <Layout>
      <section className="innerpage-wrapper">
        <div id="search-result-page">
          {/* <div className="container">
            <div className="row">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12 content-side">
                <div className="page-search-form">
                  <form className="pg-search-form">
                    <div className="row">
                      <div className="col-12 col-md-12 col-lg-4 col-xl-4">
                        <div className="form-group">
                          <label>
                            <span>
                              <i className="fa fa-map-marker"></i>
                            </span>
                            Where
                          </label>
                          <input
                            className="form-control"
                            placeholder="City, Hotel Name, Country"
                          />
                        </div>
                      </div>

                      <div className="col-12 col-md-12 col-lg-4 col-xl-4">
                        <div className="row">
                          <div className="col-6 col-md-6">
                            <div className="form-group">
                              <label>
                                <span>
                                  <i className="fa fa-calendar"></i>
                                </span>
                                Check In
                              </label>
                              <DatePicker
                                className="form-control dpd1"
                                selected={startDate}
                                onChange={(date) => setStartDate(date)}
                                minDate={new Date()}
                                showDisabledMonthNavigation
                                shouldCloseOnSelect={false}
                                placeholderText="Date"
                              />
                            </div>
                          </div>

                          <div className="col-6 col-md-6">
                            <div className="form-group">
                              <label>
                                <span>
                                  <i className="fa fa-calendar"></i>
                                </span>
                                Check Out
                              </label>
                              <DatePicker
                                className="form-control dpd1"
                                selected={endDate}
                                onChange={(date) => setEndDate(date)}
                                minDate={startDate}
                                showDisabledMonthNavigation
                                shouldCloseOnSelect={false}
                                placeholderText="Date"
                              />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="col-12 col-md-12 col-lg-4 col-xl-4">
                        <div className="row">
                          <div className="col-6 col-md-6">
                            <div className="form-group">
                              <label>
                                <span>
                                  <i className="fa fa-users"></i>
                                </span>
                                Guests
                              </label>
                              <input
                                type="number"
                                className="form-control"
                                placeholder="Total"
                                min="0"
                              />{" "}
                            </div>
                          </div>

                          <div className="col-6 col-md-6">
                            <div className="form-group">
                              <label>
                                <span>
                                  <i className="fa fa-clone"></i>
                                </span>
                                Rooms
                              </label>
                              <input
                                type="number"
                                className="form-control"
                                placeholder="Total"
                                min="0"
                              />{" "}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <button className="btn btn-orange">Search</button>
                  </form>
                </div> */}

          <SearchResult />

          {/* </div>
            </div>
          </div> */}
        </div>
      </section>
    </Layout>
  );
};

export default HotelsResult;
