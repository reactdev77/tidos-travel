import Router from "next/router";
import { useState } from "react";
import PriceForm from "../../components/hotel/booking/PriceForm";
import ServiceForm from "../../components/hotel/booking/ServiceForm";
import UserForm from "../../components/hotel/booking/UserForm";
import Layout from "../../components/layout/Layout";

const styles = {
  backBtn: {
    color: "#5452b5",
    border: "1px solid #5452b5",
    background: "white",
    marginRight: 15,
  },
};

export default function Booking() {
  const [step, setStep] = useState("");

  return (
    <Layout>
      <div className="booking">
        <div className="orangealert">
          <h2>Restrictions related to your trip</h2>
          <p>
            This destination may have COVID-19 travel restrictions in place,
            including specific restrictions for lodging. Check any national,
            local and health advisories for this destination before you book.
          </p>
        </div>
        <div className="heading">
          <h3>Complete your booking in 3 easy steps</h3>
        </div>
        <div className="checkout">
          {step !== "final" ? (
            <div className="user-form">
              <form>
                <UserForm />
                <div className="future-option">
                  <input type="checkbox" />
                  <p>Receive text alerts about this trip(Free of charge)</p>
                </div>
                <div style={{ display: "flex" }}>
                  <button
                    style={styles.backBtn}
                    type="button"
                    onClick={() => Router.back()}
                  >
                    Back
                  </button>
                  <button type="submit" onClick={() => setStep("final")}>
                    Next
                  </button>
                </div>
              </form>
            </div>
          ) : (
            <div className="service-form">
              <form>
                <ServiceForm />
                <div style={{ display: "flex" }}>
                  <button
                    style={styles.backBtn}
                    type="button"
                    onClick={() => setStep("start")}
                  >
                    Back
                  </button>
                  <button
                    type="button"
                    onClick={() => Router.push("/booking/payment")}
                  >
                    Proceed to pay
                  </button>
                </div>
              </form>
            </div>
          )}
          <div className="booking-form">
            <PriceForm />
          </div>
        </div>
      </div>
    </Layout>
  );
}
