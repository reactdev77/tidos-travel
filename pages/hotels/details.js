import Router from "next/router";
import Layout from "../../components/layout/Layout";

export default function HotelDetails() {
  return (
    <Layout>
      <div className="hotel-details_container">
        <div className="toparea_shp">
          <h1 className="maintit_shp">Herta Berlin Hotel</h1>
          <div className="row_center">
            <div className="location">
              <img src="/images/location.svg" alt="" />
              SCOTLAND
            </div>
            <div className="review">
              <img src="/images/Big_star.svg" alt="" />
              4.8(400 reviews)
            </div>
            <a href="" className="seemap">
              See Map
            </a>
          </div>
          <p className="greenalert">
            Great for families, Free buffet breakfast, free WiFi, and free
            parking. Price Guarantee
          </p>

          <div className="rightprices">
            <h2>$22,541</h2>
            <p>
              7 nights <br />
              Including Taxes amd fees
            </p>
            <a href="" className="defbtn">
              Choose room
            </a>
          </div>
        </div>
        <div className="orangealert">
          <h2>Restrictions related to your trip</h2>
          <p>
            This destination may have COVID-19 travel restrictions in place,
            including specific restrictions for lodging. Check any national,
            local and health advisories for this destination before you book.
          </p>
        </div>

        <div className="gallery_shp">
          <div className="lbox">
            <div className="img img3">
              <img src="/images/img3.png" alt="" />
              <div className="tag">Pool Side</div>
            </div>
            <div className="twoinrow">
              <div className="l">
                <div className="img img4">
                  <img src="/images/img4.png" alt="" />
                  <div className="tag">Beach View</div>
                </div>
              </div>
              <div className="r">
                <div className="img img5">
                  <img src="/images/img5.png" alt="" />
                  <div className="tag">Drone view</div>
                </div>
              </div>
            </div>
          </div>
          <div className="rbox">
            <div className="img1 img">
              <img src="/images/img1.png" alt="" />
              <div className="tag">Bedroom</div>
            </div>
            <div className="img2 img">
              <img src="/images/img2.png" alt="" />
              <a href="" className="seeallpictures">
                <div>
                  See all 47 Pictures
                  <img src="/images/galleryimg.svg" alt="" />
                </div>
              </a>
            </div>
          </div>
        </div>

        <div className="tabs">
          <div className="l">
            <a href="" className="active">
              OVERVIEW&PHOTOS
            </a>
            <a href="">ROOMS</a>
            <a href="">AMENITIES</a>
            <a href="">POLICIES</a>
            <a href="">GUEST REVIEW</a>
          </div>
          <div className="r">
            <a href="" className="defbtn">
              Choose room
            </a>
          </div>
        </div>
        <div className="tabcontent">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non
          </p>
        </div>

        <div className="facialityandothers">
          <div className="leftside">
            <div className="box">
              <h2>Hotel Facility</h2>
              <div className="stuffs">
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon1.svg" alt="" />
                  </div>
                  <span>Free Wifi</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon2.svg" alt="" />
                  </div>
                  <span>Free Parking</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon3.svg" alt="" />
                  </div>
                  <span>Airport Transport</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon4.svg" alt="" />
                  </div>
                  <span>Free Breakfast</span>
                </div>
              </div>
              <div className="showalllink">
                <a href="">
                  Show all facilities <img src="/images/arrow.svg" alt="" />
                </a>
              </div>
            </div>
          </div>
          <div className="righside">
            <div className="placeformap">
              <img src="/images/map.png" alt="" />
            </div>
          </div>
        </div>

        <div className="facialityandothers">
          <div className="leftside">
            <div className="box">
              <h2>Cleaning and safety practices</h2>
              <div className="stuffs">
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon5.svg" alt="" />
                  </div>
                  <span>Cleaned with disinfectants</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon6.svg" alt="" />
                  </div>
                  <span>Social distancing</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon5.svg" alt="" />
                  </div>
                  <span>Cleaned with disinfectants</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon6.svg" alt="" />
                  </div>
                  <span>Social distancing</span>
                </div>
              </div>
              <div className="showalllink">
                <a href="">
                  Show all Safety <img src="/images/arrow.svg" alt="" />
                </a>
              </div>
            </div>
          </div>
          <div className="righside">
            <div className="box">
              <h2>Landmarks to explore in the area</h2>
              <div className="stuffs">
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon7.svg" alt="" />
                  </div>
                  <span className="witsubtext">
                    National Museum{" "}
                    <span className="onright">30 mins walk</span>
                  </span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon7.svg" alt="" />
                  </div>
                  <span className="witsubtext">
                    National Museum{" "}
                    <span className="onright">30 mins walk</span>
                  </span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon7.svg" alt="" />
                  </div>
                  <span className="witsubtext">
                    National Museum{" "}
                    <span className="onright">30 mins walk</span>
                  </span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon7.svg" alt="" />
                  </div>
                  <span className="witsubtext">
                    National Museum{" "}
                    <span className="onright">30 mins walk</span>
                  </span>
                </div>
              </div>
              <div className="showalllink">
                <a href="">
                  Show all Landmarks <img src="/images/arrow.svg" alt="" />
                </a>
              </div>
            </div>
          </div>
        </div>
        <h2 className="title">Choose your room</h2>
        <div className="room_cards">
          <div className="room_card">
            <div className="box">
              <a href="">
                <div className="placeforimg">
                  <img src="/images/room.png" alt="" />
                  <div className="gallery">
                    <img src="/images/galleryimg.svg" alt="" /> 10
                  </div>
                </div>
              </a>
              <h2>Superior Room</h2>
              <div className="tags">
                <div className="tag">Mountain view</div>
              </div>
              <div className="ability">
                <div className="pl">
                  <img src="/images/icon8.svg" alt="" />
                </div>
                2 People
              </div>
              <div className="ability">
                <div className="pl">
                  <img src="/images/icon9.svg" alt="" />
                </div>
                2 Twin bed
              </div>
              <a href="" className="showinfo">
                SHOW ROOM INFORMATION
              </a>
            </div>
            <div className="box oppay">
              <h2>Options</h2>
              <ul>
                <li>
                  Cancellation Policy <img src="/images/icon10.svg" alt="" />
                </li>
                <li>Breakfast for 2 People</li>
              </ul>
            </div>
            <div className="box oppay">
              <h2>Payment</h2>
              <ul>
                <li>
                  TIDOS Coin <img src="/images/icon10.svg" alt="" />
                </li>
                <li>Metamask</li>
              </ul>
            </div>
            <div className="box boxprice">
              <h2>Today’s price (Taxes and Fees)</h2>
              <div className="prices">
                <h3>$22,541</h3>
                <p>7 nights, 1 room</p>
                <p className="redflag">We have 3 left</p>
              </div>
              <div className="btn">
                <a
                  onClick={() => Router.push("/hotels/booking")}
                  className="defbtn"
                >
                  Reserve room
                </a>
              </div>
            </div>
          </div>
          <div className="room_card">
            <div className="box">
              <a href="">
                <div className="placeforimg">
                  <img src="/images/room.png" alt="" />
                  <div className="gallery">
                    <img src="/images/galleryimg.svg" alt="" /> 10
                  </div>
                </div>
              </a>
              <h2>Superior Room</h2>
              <div className="tags">
                <div className="tag">Mountain view</div>
              </div>
              <div className="ability">
                <div className="pl">
                  <img src="/images/icon8.svg" alt="" />
                </div>
                2 People
              </div>
              <div className="ability">
                <div className="pl">
                  <img src="/images/icon9.svg" alt="" />
                </div>
                2 Twin bed
              </div>
              <a href="" className="showinfo">
                SHOW ROOM INFORMATION
              </a>
            </div>
            <div className="box oppay">
              <h2>Options</h2>
              <ul>
                <li>
                  Cancellation Policy <img src="/images/icon10.svg" alt="" />
                </li>
                <li>Breakfast for 2 People</li>
                <li>Free Parking</li>
                <li>1 Child stay free</li>
              </ul>
            </div>
            <div className="box oppay">
              <h2>Payment</h2>
              <ul>
                <li>
                  TIDOS Coin <img src="/images/icon10.svg" alt="" />
                </li>
                <li>Metamask</li>
              </ul>
            </div>
            <div className="box boxprice">
              <h2>Today’s price (Taxes and Fees)</h2>
              <div className="prices">
                <h3>$22,541</h3>
                <p>7 nights, 1 room</p>
                <p className="redflag">Sold out</p>
              </div>
              <div className="btn">
                <a
                  onClick={() => Router.push("/hotels/booking")}
                  className="defbtn"
                >
                  Reserve room
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="amentiests">
          <h2 className="title">Amenities</h2>
          <div className="boxes">
            <div className="box">
              <div className="stuffs">
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon1.svg" alt="" />
                  </div>
                  <span>Free Wifi</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon3.svg" alt="" />
                  </div>
                  <span>Airport Transport</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon1.svg" alt="" />
                  </div>
                  <span>Free Wifi</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon3.svg" alt="" />
                  </div>
                  <span>Airport Transport</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon1.svg" alt="" />
                  </div>
                  <span>Free Wifi</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon3.svg" alt="" />
                  </div>
                  <span>Airport Transport</span>
                </div>
              </div>
            </div>
            <div className="box">
              <div className="stuffs">
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon2.svg" alt="" />
                  </div>
                  <span>Free Parking</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon4.svg" alt="" />
                  </div>
                  <span>Free Breakfast</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon2.svg" alt="" />
                  </div>
                  <span>Free Parking</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon4.svg" alt="" />
                  </div>
                  <span>Free Breakfast</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon2.svg" alt="" />
                  </div>
                  <span>Free Parking</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon4.svg" alt="" />
                  </div>
                  <span>Free Breakfast</span>
                </div>
              </div>
            </div>
            <div className="box">
              <div className="stuffs">
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon2.svg" alt="" />
                  </div>
                  <span>Free Parking</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon4.svg" alt="" />
                  </div>
                  <span>Free Breakfast</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon2.svg" alt="" />
                  </div>
                  <span>Free Parking</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon4.svg" alt="" />
                  </div>
                  <span>Free Breakfast</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon2.svg" alt="" />
                  </div>
                  <span>Free Parking</span>
                </div>
                <div className="stuff">
                  <div className="plimg">
                    <img src="/images/icon4.svg" alt="" />
                  </div>
                  <span>Free Breakfast</span>
                </div>
              </div>
            </div>
          </div>
          <div className="showalllink">
            <a href="">
              Show all facilities <img src="/images/arrow.svg" alt="" />
            </a>
          </div>
        </div>
        <div className="policies">
          <h2 className="title">Policies</h2>
          <div className="boxes">
            <div className="box">
              <h3>Check-in</h3>
              <ul>
                <li>Check-in from 2:00 PM - 8:00 PM</li>
                <li>Minimum check-in age - 18</li>
              </ul>
            </div>
            <div className="box">
              <h3>Check-out</h3>
              <ul>
                <li>Check-out before 11 A</li>
              </ul>
            </div>
            <div className="box">
              <h3>Special check-in instructions</h3>
              <ul>
                <li>The front desk is open daily from 8:00 AM - 8:00 PM</li>
                <li>{`This property doesn't offer after-hours check-in`}</li>
                <li>Front desk staff will greet guests on arrival</li>
                <li>
                  The Economy Single Rooms are on the top 2 floors, accessible
                  only by stairs.
                </li>
              </ul>
            </div>
          </div>
          <div className="boxes">
            <div className="box">
              <h3>Renovations and closures</h3>
              <ul>
                <li>
                  The property is closed between 20 <br /> December and 30
                  December.
                </li>
              </ul>
            </div>
            <div className="box">
              <h3>Access methods</h3>
              <ul>
                <li>Staffed front desk</li>
              </ul>
            </div>
            <div className="box">
              <h3>Pets</h3>
              <ul>
                <li>Pets not allowed</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="guessreview">
          <h2 className="title">Guest Review</h2>
          <h3>
            4.7/5 <span>Based on 17, 222 reviews</span>
          </h3>

          <div className="boxes">
            <div className="box">
              <div className="line">
                <div className="gray_l">
                  <div className="blue_l" style={{ width: "81%" }}></div>
                </div>
                <p>4.9 (15,000)</p>
              </div>
              <div className="line">
                <div className="gray_l">
                  <div className="blue_l" style={{ width: "53%" }}></div>
                </div>
                <p>4.4 (1,000)</p>
              </div>
              <div className="line">
                <div className="gray_l">
                  <div className="blue_l" style={{ width: "42%" }}></div>
                </div>
                <p>3.4 (100)</p>
              </div>
              <div className="line">
                <div className="gray_l">
                  <div className="blue_l" style={{ width: "42%" }}></div>
                </div>
                <p>3.4 (10)</p>
              </div>
              <div className="line">
                <div className="gray_l">
                  <div className="blue_l" style={{ width: "42%" }}></div>
                </div>
                <p>3.4 (2)</p>
              </div>
            </div>
            <div className="box">
              <div className="marks">
                <div className="mark">
                  <h2>4.6/5</h2>
                  <p>Cleanliness</p>
                </div>
                <div className="mark">
                  <h2>4.7/5</h2>
                  <p>Staff & service</p>
                </div>
              </div>
              <div className="marks">
                <div className="mark">
                  <h2>4.7/5</h2>
                  <p>Amenities</p>
                </div>
                <div className="mark">
                  <h2>4.3/5</h2>
                  <p>Property facilities</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="reviews">
          <div className="review">
            <div className="cont">
              <h2>4.9/5</h2>
              <h3>Abel</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Dui
              </p>
              <p className="date">Date of stay: 20/3/2022</p>
              <p className="date">Date of review: 30/3/2022</p>
            </div>
          </div>
          <div className="review">
            <div className="cont">
              <h2>4.9/5</h2>
              <h3>Abel</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Dui
              </p>
              <p className="date">Date of stay: 20/3/2022</p>
              <p className="date">Date of review: 30/3/2022</p>
            </div>
          </div>
          <div className="review">
            <div className="cont">
              <h2>4.9/5</h2>
              <h3>Abel</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Dui
              </p>
              <p className="date">Date of stay: 20/3/2022</p>
              <p className="date">Date of review: 30/3/2022</p>
            </div>
          </div>
          <div className="review">
            <div className="cont">
              <h2>4.9/5</h2>
              <h3>Abel</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Dui
              </p>
              <p className="date">Date of stay: 20/3/2022</p>
              <p className="date">Date of review: 30/3/2022</p>
            </div>
          </div>
        </div>
        <div className="btn_review">
          <a href="" className="defbtn">
            See all reviews
          </a>
        </div>
      </div>
    </Layout>
  );
}
