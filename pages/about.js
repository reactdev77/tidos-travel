import Link from "next/link";
import Layout from "../components/layout/Layout";

export default function About() {
  return (
    <Layout>
      <section className="page-cover" id="cover-login">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <h1 className="page-title">About Us Page</h1>
              <ul className="breadcrumb">
                <li>
                  <Link href="/">Home</Link>
                </li>
                <li className="active">About Us Page</li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section className="innerpage-wrapper">
        <div id="about-us">
          <div id="about-content" className="section-padding">
            <div className="container">
              <div className="row">
                <div className="col-sm-12">
                  <div className="flex-content">
                    <div className="flex-content-img about-img">
                      {/* <img
                        src="/public/images/about-us.jpg"
                        className="img-responsive"
                        alt="about-img"
                      /> */}
                    </div>

                    <div className="about-text">
                      <div className="about-detail">
                        <h2>About Star Travels</h2>
                        <p>
                          Lorem ipsum dolor sit amet, conse adipiscing elit.
                          Curabitur metus felis, venenatis eu ultricies vel,
                          vehicula eu urna. Phasellus eget augue id est
                          fringilla feugiat id a tellus. Sed hendrerit quam sed
                          ante euismod posuere ultricies. Vestibulum suscipit
                          convallis purus ut mattis. In eget turpis eget urna
                          molestie ultricies in sagittis nunc. Sed accumsan leo
                          in mauris rhoncus volutpat.
                        </p>
                        <p>
                          Iuvaret detraxit disputando vel ea, ut virtute
                          per.Lorem ipsum dolor si Iuvaret detraxit. disputando
                          velr.Lorem ipsum dolor si. Lorem ipsum dolor sit amet,
                          ad duo fugit aeque fabulas, pro an eros perpetua
                          ullamcorper.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div id="video-banner" className="banner-padding back-size">
            <div className="container">
              <div className="row">
                <div className="col-sm-12">
                  <h2>Take a Video Tour</h2>
                  <p>
                    Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in
                    lucilius prodesset pri. Veniam delectus ei vis. Est atqui
                    timeam mnesarchum at, pro an eros perpetua ullamcorper.
                  </p>

                  <a href="" className="popup-youtube" id="play-button">
                    <span>
                      <i className="fa fa-play"></i>
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div id="team" className="section-padding">
            <div className="container">
              <div className="row">
                <div className="col-sm-12">
                  <div className="page-heading">
                    <h2>Meet Our Team</h2>
                    <hr className="heading-line" />
                  </div>

                  <div className="owl-carousel owl-theme" id="owl-team">
                    <div className="item">
                      <div className="member-block">
                        <div className="member-img">
                          {/* <img
                            src="/public/images/team-member-1.jpg"
                            className="img-responsive img-circle"
                            alt="member-img"
                          /> */}
                          <ul className="list-unstyled list-inline contact-links">
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-facebook-square"></i>
                                </span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-twitter-square"></i>
                                </span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-linkedin-square"></i>
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>

                        <div className="member-name">
                          <h3>John Doe</h3>
                          <p>Director</p>
                        </div>
                      </div>
                    </div>

                    <div className="item">
                      <div className="member-block">
                        <div className="member-img">
                          {/* <img
                            src="/public/images/team-member-2.jpg"
                            className="img-responsive img-circle"
                            alt="member-img"
                          /> */}
                          <ul className="list-unstyled list-inline contact-links">
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-facebook-square"></i>
                                </span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-twitter-square"></i>
                                </span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-linkedin-square"></i>
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>

                        <div className="member-name">
                          <h3>John Doe</h3>
                          <p>Director</p>
                        </div>
                      </div>
                    </div>

                    <div className="item">
                      <div className="member-block">
                        <div className="member-img">
                          {/* <img
                            src="/public/images/team-member-3.jpg"
                            className="img-responsive img-circle"
                            alt="member-img"
                          /> */}
                          <ul className="list-unstyled list-inline contact-links">
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-facebook-square"></i>
                                </span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-twitter-square"></i>
                                </span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-linkedin-square"></i>
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>

                        <div className="member-name">
                          <h3>John Doe</h3>
                          <p>Director</p>
                        </div>
                      </div>
                    </div>

                    <div className="item">
                      <div className="member-block">
                        <div className="member-img">
                          {/* <img
                            src="/public/images/team-member-4.jpg"
                            className="img-responsive img-circle"
                            alt="member-img"
                          /> */}
                          <ul className="list-unstyled list-inline contact-links">
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-facebook-square"></i>
                                </span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-twitter-square"></i>
                                </span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-linkedin-square"></i>
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>

                        <div className="member-name">
                          <h3>John Doe</h3>
                          <p>Director</p>
                        </div>
                      </div>
                    </div>

                    <div className="item">
                      <div className="member-block">
                        <div className="member-img">
                          {/* <img
                            src="/public/images/team-member-5.jpg"
                            className="img-responsive img-circle"
                            alt="member-img"
                          /> */}
                          <ul className="list-unstyled list-inline contact-links">
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-facebook-square"></i>
                                </span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-twitter-square"></i>
                                </span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <span>
                                  <i className="fa fa-linkedin-square"></i>
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>

                        <div className="member-name">
                          <h3>John Doe</h3>
                          <p>Director</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}
