import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";

import Layout from "../../components/layout/Layout";
import { useEffect } from "react";

const ForgotPassword = () => {
  const router = useRouter();
  // const dispatch = useDispatch();
  const user = useSelector((state) => state.user);

  useEffect(() => {
    if (user?.isLoggedIn) {
      router.back();
    }
  });

  // form validation rules
  const validationOptions = {
    email: { required: "Email is required" },
  };

  // get functions to build form with useForm() hook
  const { register, handleSubmit, formState } = useForm();
  const { errors } = formState;

  async function onSubmit({ email }) {
    // get return url from query parameters or default to '/'
  }

  return (
    <>
      {!user?.isLoggedIn ? (
        <Layout>
          <section className="innerpage-wrapper">
            <div id="forgot-password" className="innerpage-section-padding">
              <div className="container">
                <div className="row">
                  <div className="col-sm-12">
                    <div className="flex-content">
                      <div className="custom-form custom-form-fields">
                        <h3>Forgot Password</h3>
                        <form onSubmit={handleSubmit(onSubmit)}>
                          <div className="form-group">
                            <input
                              name="email"
                              type="text"
                              {...register("email", validationOptions.email)}
                              className="form-control"
                              placeholder="Your Email"
                            />
                            <span>
                              <i className="fa fa-envelope"></i>
                            </span>
                            <p className="invalid-feedback">
                              {errors.email?.message}
                            </p>
                          </div>

                          <button className="btn btn-orange btn-block">
                            Send
                          </button>
                        </form>
                      </div>

                      <div className="flex-content-img custom-form-img">
                        <img
                          src="/public/images/forgot-password.jpg"
                          className="img-responsive"
                          alt="registration-img"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </Layout>
      ) : (
        <></>
      )}
    </>
  );
};

export default ForgotPassword;
