import Link from "next/link";

import Layout from "../components/layout/Layout";

export default function Contact() {
  return (
    <Layout>
      <section className="page-cover" id="cover-login">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <h1 className="page-title">Contact Us Page</h1>
              <ul className="breadcrumb">
                <li>
                  <Link href="/">Home</Link>
                </li>
                <li className="active">Contact Us Page</li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section className="innerpage-wrapper">
        <div id="contact-us" className="innerpage-section-padding">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 col-md-5 no-pd-r">
                <div className="custom-form contact-form">
                  <h3>Contact Us</h3>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et.
                  </p>
                  <form>
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Name"
                        required
                      />
                      <span>
                        <i className="fa fa-user"></i>
                      </span>
                    </div>

                    <div className="form-group">
                      <input
                        type="email"
                        className="form-control"
                        placeholder="Email"
                        required
                      />
                      <span>
                        <i className="fa fa-envelope"></i>
                      </span>
                    </div>

                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Subject"
                        required
                      />
                      <span>
                        <i className="fa fa-info-circle"></i>
                      </span>
                    </div>

                    <div className="form-group">
                      <textarea
                        className="form-control"
                        rows="4"
                        placeholder="Your Message"
                      ></textarea>
                      <span>
                        <i className="fa fa-pencil"></i>
                      </span>
                    </div>

                    <button className="btn btn-orange btn-block">Send</button>
                  </form>
                </div>
              </div>

              <div className="col-sm-12 col-md-7 no-pd-l">
                <div className="map">
                  <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d184552.57289400208!2d-79.51814088466423!3d43.718155663242015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d4cb90d7c63ba5%3A0x323555502ab4c477!2sToronto%2C%20ON!5e0!3m2!1sen!2sca!4v1647233844626!5m2!1sen!2sca"
                    allowFullScreen
                  ></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}
