import Link from "next/link";
import { useState } from "react";
import { useSelector } from "react-redux";
import DatePicker from "react-datepicker";

import Layout from "../../components/layout/Layout";
import FlightSearchResult from "../../components/flight/search";

const FlightsResult = () => {
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [trip, setTrip] = useState("single-trip");

  return (
    <Layout>
      <section className="innerpage-wrapper">
        <div id="search-result-page">
          {/* <div className="container">
            <div className="row">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12 content-side">
                <div className="page-search-form">
                  <ul className="nav nav-tabs">
                    <li
                      className={`nav-item ${
                        trip === "single-trip" && "active"
                      }`}
                    >
                      <a
                        className="nav-link"
                        style={{ cursor: "pointer" }}
                        data-toggle="tab"
                        onClick={() => setTrip("single-trip")}
                      >
                        One way
                      </a>
                    </li>
                    <li
                      className={`nav-item ${
                        trip === "round-trip" && "active"
                      }`}
                    >
                      <a
                        className="nav-link"
                        style={{ cursor: "pointer" }}
                        data-toggle="tab"
                        onClick={() => setTrip("round-trip")}
                      >
                        Return
                      </a>
                    </li>
                  </ul>

                  <div className="tab-content">
                    <div
                      id="tab-round-trip"
                      className={`tab-pane in ${
                        trip === "round-trip" && "active"
                      }`}
                    >
                      <form className="pg-search-form">
                        <div className="row">
                          <div className="col-12 col-md-6 col-lg-3 col-xl-3">
                            <div className="form-group">
                              <label>
                                <span>
                                  <i className="fa fa-map-marker"></i>
                                </span>
                                From
                              </label>
                              <input
                                className="form-control"
                                placeholder="Destination, City, Country"
                              />
                            </div>
                          </div>

                          <div className="col-12 col-md-6 col-lg-3 col-xl-3">
                            <div className="form-group">
                              <label>
                                <span>
                                  <i className="fa fa-map-marker"></i>
                                </span>
                                To
                              </label>
                              <input
                                className="form-control"
                                placeholder="Destination, City, Country"
                              />
                            </div>
                          </div>

                          <div className="col-12 col-md-12 col-lg-4 col-xl-4">
                            <div className="row">
                              <div className="col-6 col-md-6">
                                <div className="form-group">
                                  <label>
                                    <span>
                                      <i className="fa fa-calendar"></i>
                                    </span>
                                    Departing
                                  </label>
                                  <DatePicker
                                    className="form-control dpd1"
                                    selected={startDate}
                                    onChange={(date) => setStartDate(date)}
                                    minDate={new Date()}
                                    showDisabledMonthNavigation
                                    shouldCloseOnSelect={false}
                                    placeholderText="Date"
                                  />
                                </div>
                              </div>

                              <div className="col-6 col-md-6">
                                <div className="form-group">
                                  <label>
                                    <span>
                                      <i className="fa fa-calendar"></i>
                                    </span>
                                    Returning
                                  </label>
                                  <DatePicker
                                    className="form-control dpd1"
                                    selected={endDate}
                                    onChange={(date) => setEndDate(date)}
                                    minDate={startDate}
                                    showDisabledMonthNavigation
                                    shouldCloseOnSelect={false}
                                    placeholderText="Date"
                                  />
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="col-12 col-md-12 col-lg-2 col-xl-2">
                            <div className="form-group">
                              <label>
                                <span>
                                  <i className="fa fa-users"></i>
                                </span>
                                Passengers
                              </label>
                              <input
                                type="number"
                                className="form-control"
                                placeholder="Total"
                                min="0"
                              />{" "}
                            </div>
                          </div>
                        </div>

                        <button className="btn btn-orange">Search</button>
                      </form>
                    </div>

                    <div
                      id="tab-one-way"
                      className={`tab-pane in ${
                        trip === "single-trip" && "active"
                      }`}
                    >
                      <form className="pg-search-form">
                        <div className="row">
                          <div className="col-12 col-md-6 col-lg-3 col-xl-3">
                            <div className="form-group">
                              <label>
                                <span>
                                  <i className="fa fa-map-marker"></i>
                                </span>
                                From
                              </label>
                              <input
                                className="form-control"
                                placeholder="Destination, City, Country"
                              />
                            </div>
                          </div>

                          <div className="col-12 col-md-6 col-lg-3 col-xl-3">
                            <div className="form-group">
                              <label>
                                <span>
                                  <i className="fa fa-map-marker"></i>
                                </span>
                                To
                              </label>
                              <input
                                className="form-control"
                                placeholder="Destination, City, Country"
                              />
                            </div>
                          </div>

                          <div className="col-12 col-md-6 col-lg-3 col-xl-3">
                            <div className="form-group">
                              <label>
                                <span>
                                  <i className="fa fa-calendar"></i>
                                </span>
                                Departing
                              </label>
                              <input
                                className="form-control dpd3"
                                placeholder="Date"
                              />
                            </div>
                          </div>

                          <div className="col-12 col-md-6 col-lg-3 col-xl-3">
                            <div className="form-group">
                              <label>
                                <span>
                                  <i className="fa fa-users"></i>
                                </span>
                                Passengers
                              </label>
                              <input
                                type="number"
                                className="form-control"
                                placeholder="Total"
                              />{" "}
                            </div>
                          </div>
                        </div>

                        <button className="btn btn-orange">Search</button>
                      </form>
                    </div>
                  </div>
                </div>

                <div className="row"> */}
          <FlightSearchResult />
          {/* </div>
              </div>
            </div>
          </div> */}
        </div>
      </section>
    </Layout>
  );
};

export default FlightsResult;
