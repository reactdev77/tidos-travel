import Banner from "../../components/home/Banner";
import Destination from "../../components/home/Destination";
import FAQ from "../../components/home/Fag";
import Features from "../../components/home/Features";
import Feedback from "../../components/home/Feedback";
import NewsLetter from "../../components/home/NewsLetter";
import Partner from "../../components/home/Partner";
import Layout from "../../components/layout/Layout";

export default function Flights() {
  return (
    <Layout>
      <Banner />
      <Destination />
      <Features />
      <Feedback />
      <Partner />
      <FAQ />
      <NewsLetter />
    </Layout>
  );
}
