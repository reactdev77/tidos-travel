import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Popup from "reactjs-popup";
import "reactjs-popup/dist/index.css";

import Layout from "../../components/layout/Layout";
import Login from "../../components/account/Login";
import Register from "../../components/account/Register";
import styles from "./Booking.module.css";

export default function Reservation() {
  const [page, setPage] = useState("login");
  const user = useSelector((state) => state.user);

  return (
    <Layout user={user}>
      {!user.isLoggedIn ? (
        <Popup trigger={<a className="button">Login</a>} modal nested>
          {(close) => (
            <div>
              <button className={styles.close} onClick={close}>
                &times;
              </button>
              {page === "login" ? (
                <Login setPage={setPage} close={close} />
              ) : (
                <Register setPage={setPage} />
              )}
            </div>
          )}
        </Popup>
      ) : (
        <div>You logged in</div>
      )}
    </Layout>
  );
}
