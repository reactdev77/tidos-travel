import Layout from "../../../components/layout/Layout";

export default function HotelReceipt() {
    return (
        <Layout>
            <div className="wrapper">

<div className="page-head-container">
    <div className="page-head">
        <div className="tdo-icon"></div>
        <div className="heading">Booking Confirmed!</div>
        <div className="sub-header">Receipt for your booking</div>
    </div>
</div>




    <div className="page-content-container">

   
        <div className="page-content-title">
            <div className="user-detail">
                <div className="user-name">Hello, John!</div>
                <p>
                    Thank you for booking with us. Here are the details of your booking.
                            </p>
                            <p>We hope to see you again soon. </p>
            </div>
            <div className="hotel-detail">
                <div className="tdo-icon"></div>
                <div className="hotel-name">Herta Berlin Hotel</div>
            </div>
            <div className="download-btn">
                <button className="receipt_btn btn-soft-primary">Download Receipt</button>
            </div>
        </div>

        <div className="booking-content">
            <div className="booking-detail">
                <div className="booking-info">
                    Booking date
                </div>
                <div className="booking-description">
                    18 March 2022
                </div>
            </div>
            <div className="booking-detail">
                <div className="booking-info">
                    Invoice Number
                </div>
                <div className="booking-description">
                    #24244321
                </div>
            </div>
            <div className="booking-detail">
                <div className="booking-info">
                    Payment Method
                </div>
                <div className="booking-description">
                    Tidos
                </div>
            </div>
            <div className="booking-detail">
                <div className="booking-info">
                    Hotel Address
                </div>
                <div className="booking-description">
                    300 Walnut wood lane, NY 12660
                </div>
            </div>
            <div className="booking-detail">
                <div className="booking-info">
                    Recipient
                </div>
                <div className="booking-description">
                    Mr. Tom Jones
                </div>
            </div>
            <div className="booking-detail">
                <div className="booking-info">
                    Room Number
                </div>
                <div className="booking-description">
                    202
                </div>
            </div>
            <div className="booking-detail">
                <div className="booking-info">
                    Check-in
                </div>
                <div className="booking-description">
                    22/03/2022
                </div>
            </div>
            <div className="booking-detail">
                <div className="booking-info">
                    Check-out
                </div>
                <div className="booking-description">
                    30/03/2022
                </div>
            </div>
        </div>

        <div className="payment-info">
            <div className="table-responive">
                <table className="table">
                    <tr>
                        <th>Description</th>
                        <th>Unit Cost</th>
                        <th>Nights</th>
                        <th>Rooms</th>
                        <th>Amount</th>
                    </tr>
                    <tr>
                        <td>Superior Room,Bathtub(Mountain view)</td>
                        <td>$152.44</td>
                        <td>1</td>
                        <td>1</td>
                        <td>$152.44</td>
                    </tr>
                    <tr>
                        <td>Superior Room,Bathtub(Mountain view)</td>
                        <td>$152.44</td>
                        <td>1</td>
                        <td>1</td>
                        <td>$152.44</td>
                    </tr>
                </table>
            </div>
            <div className="payment-total">
                <ul>
                    <li></li>
                    <li>Subtotal</li>
                    <li>$152.44 (15.22 TDO)</li>
                </ul>
                <ul>
                    <li></li>
                    <li>Taxes and Fees</li>
                    <li>$22.44 (2.244 TDO)</li>
                </ul>
                <ul className="total">
                    <li></li>
                    <li>Total</li>
                    <li className="total-amount">$174.88 (17.488 TDO)</li>
                </ul>
            </div>
        </div>

        <div className="download-receipt">
            <button className="receipt_btn receipt_btn-lg receipt_btn-primary">Download Receipt</button>
        </div>

        <div className="address-info">
            <ul>
                <li>
                    <img src="/images/icons/location.png" alt="location" />
                    <span>Block 1, dash dash street, Toronto, Canada</span>
                </li>
                <li>
                    <img src="/images/icons/world.png" alt="world" />
                    <span><a href="#">Tidos.Travel</a></span>
                </li>
            </ul>
        </div>


    </div>


</div>
        </Layout>
    )
}