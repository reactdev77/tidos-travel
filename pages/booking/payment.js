import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Layout from "../../components/layout/Layout";
import { useWeb3Context } from "../../web3/providers/Web3ContextProvider";
import useBalance from "../../hooks/useBalance";
import styles from "./Booking.module.css";

export default function Payment() {
  const user = useSelector((state) => state.user);
  const { address, chainId, isConnected, connect } = useWeb3Context();
  const { balance, tidosBalance } = useBalance(address, chainId);
  const [method, setMethod] = useState("built-in");
  const [amount, setAmount] = useState(10);
  const [txNumber, setTxNumber] = useState(null);

  useEffect(() => {
    const randomNumber = `TD${Math.floor(100000 + Math.random() * 900000)}`;
    if (txNumber === null) setTxNumber(randomNumber);
  }, [txNumber]);

  const PayButton = () => {
    if (method === "metamask") {
      if (isConnected) {
        if (chainId === 56) {
          if (balance < amount) {
            return (
              <button disabled className={styles.disabled}>
                Insufficient funds
              </button>
            );
          } else {
            return <button>Proceed</button>;
          }
        } else {
          return (
            <>
              <div className="alert alert-danger" role="alert">
                We accept only Tidos token on Binance Smart Chain, please make
                sure you connected to BSC chain.
              </div>
              <a
                onClick={connect}
                style={{
                  cursor: "pointer",
                  textDecorationLine: "none",
                }}
              >
                Switch to Binance Smart Chain
              </a>
            </>
          );
        }
      } else {
        return <button onClick={connect}>Connect to metamask</button>;
      }
    } else {
      if (tidosBalance > amount) {
        return <button>Proceed</button>;
      } else {
        return (
          <button disabled className={styles.disabled}>
            Insufficient funds
          </button>
        );
      }
    }
  };

  return (
    <Layout user={user}>
      <div className={styles.onlinepayment}>
        <div className={styles.box}>
          <div className={styles.leftside}>
            <div className={styles.cont}>
              <h1>Seemless online payments, with digital currencies</h1>
              <p>
                We accept only tidos token on BSC chain, please make sure you
                pay with tidos token.
              </p>
            </div>
            <img
              src="/images/donwlogo.png"
              className={styles.donwlogo}
              alt=""
            />
          </div>
          <div className={styles.rightside}>
            <h2>
              To complete your payment you need to transfer $100 and network
              fees, with any crypto you choose.
            </h2>
            <form>
              <div className={styles.card}>
                <p>Select Payment Method</p>
                <select
                  value={method}
                  onChange={(e) => setMethod(e.target.value)}
                >
                  <option value="built-in">Built-in wallet</option>
                  <option value="metamask">Metamask</option>
                </select>
              </div>
              <div className={styles.card}>
                <p>Wallet Address </p>
                <input
                  type="text"
                  disabled
                  value={
                    method === "metamask"
                      ? isConnected
                        ? address
                        : ""
                      : user.userInfor?.address
                  }
                />
              </div>
              <div className={styles.card}>
                <p>Balance</p>
                <input
                  type="text"
                  disabled
                  value={`${
                    method === "metamask"
                      ? isConnected && chainId === 56
                        ? balance
                        : "0"
                      : tidosBalance
                  } TDO`}
                />
              </div>
              <div className={styles.card}>
                <p>Amount</p>
                <input type="text" disabled value={`${amount} TDO`} />
              </div>
              <div className={styles.card}>
                <p>Description</p>
                <input
                  type="text"
                  disabled
                  value="2 Bedroom hotel at Toronto, Canada"
                />
              </div>
              <div className={styles.card}>
                <p>Your Transaction Number</p>
                <div className={styles.foricon}>
                  <input type="text" disabled value={txNumber} />
                  <a className={styles.copy}>
                    <img src="/images/copy.png" alt="img_copy" />
                  </a>
                </div>
              </div>
            </form>
            <PayButton />
          </div>
        </div>
      </div>
    </Layout>
  );
}
