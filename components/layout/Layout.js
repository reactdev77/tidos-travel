import { useSelector } from "react-redux";
import Header from "./Header";
import Footer from "./Footer";

export default function Layout({ children }) {
  const user = useSelector((state) => state.user);

  return (
    <>
      <Header userInfor={user?.userInfor} isLoggedIn={user?.isLoggedIn} />
      {children}
      <Footer />
    </>
  );
}
