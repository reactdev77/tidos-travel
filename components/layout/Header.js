import $ from "jquery";
import Link from "next/link";
import { useEffect, useState } from "react";
import Popup from "reactjs-popup";
import { isMobile } from "react-device-detect";

import Login from "../account/Login";
import Register from "../account/Register";
import { useWeb3Context } from "../../web3/providers/Web3ContextProvider";
import Router from "next/router";

const styles = {
  closeButton: {
    cursor: "pointer",
    color: "#5452b5",
    position: "absolute",
    display: "block",
    padding: isMobile ? "2px 4px 0px" : "1px 5px 5px",
    lineHeight: "20px",
    right: "-10px",
    top: "-10px",
    fontSize: "24px",
    background: "#ffffff",
    borderRadius: "18px",
    border: "1px solid #cfcece",
    zIndex: 10,
  },
  loginText: {
    cursor: "pointer",
    textDecorationLine: "none",
  },
};

export default function Header(props) {
  const [userInfor, setUserInfor] = useState();
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [page, setPage] = useState("login");
  const { address, connect, disconnect, isConnected } = useWeb3Context();

  useEffect(() => {
    const { isLoggedIn, userInfor } = props;
    const isConnected = localStorage.getItem("isConnected");
    setUserInfor(userInfor);
    setIsLoggedIn(isLoggedIn);

    if (isConnected && !address) connect();
  }, [address, connect, props]);

  const handleHamburBtn = () => {
    $(".header").addClass("header_active");
    $("body").addClass("disable_scroll");
  };

  const closeSideBar = () => {
    $(".header").removeClass("header_active");
    $("body").removeClass("disable_scroll");
  };

  const handleWalletBtn = () => {
    $(".header").removeClass("header_active");
    $("body").removeClass("disable_scroll");
    isConnected ? disconnect() : connect();
  };

  const handleClick = url => {
    Router.push(url);
    closeSideBar();
  }

  return (
    <div className="header_full_width">
      <div className="header">
        <div onClick={handleHamburBtn} className="lines">
          <div className="line"></div>
          <div className="line"></div>
          <div className="line"></div>
        </div>
        <div className="logo">
          <a onClick={() => Router.push("/")}>{`TIDOS TRAVELS`}</a>
        </div>
        <div className="formobile">
          <div
            className="formobile_opened_bg_black"
            onClick={closeSideBar}
          ></div>
          <div className="inside">
            <div className="formobilelogo">
              <Link href="/" passHref>
                <img src="/images/header/logo.png" alt="logo img" />
              </Link>
              <a onClick={closeSideBar} className="xicon">
                <img src="/images/header/xicon.svg" alt="close img" />
              </a>
            </div>
            <ul>
              <li>
                <a onClick={() => handleClick("/")}>Travel</a>
              </li>
              <li>
                <a onClick={() => handleClick("/")}>Hotels</a>
              </li>
              <li>
                <a onClick={() => handleClick("/flights")}>Flights</a>
              </li>
            </ul>
            <div className="rightbtns">
              <select name="" id="">
                <option value="">EN</option>
                <option value="">FR</option>
              </select>
              {isLoggedIn && (
                //   <Link href="/user/account" className="desktop_user" passHref>
                <a className="defbtn">{userInfor?.name}</a>
                //   </Link>
              )}
              {!isLoggedIn && (
                <Popup trigger={<a className="defbtn">Login</a>} modal nested>
                  {(close) => (
                    <div>
                      <button style={styles.closeButton} onClick={close}>
                        &times;
                      </button>
                      {page === "login" ? (
                        <Login setPage={setPage} close={close} />
                      ) : (
                        <Register setPage={setPage} />
                      )}
                    </div>
                  )}
                </Popup>
              )}

              {isLoggedIn && (
                <a className="defbtn" onClick={handleWalletBtn}>{`${
                  isConnected ? "Disconnect" : "Connect"
                }`}</a>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
