import { useRouter } from "next/router";

export default function Footer() {
  const router = useRouter();

  return (
    <div className="footer">
      <div className="wrapper">
        <div className="foter_area1">
          <div className="col1">
            <img src="/images/footerlogo.png" alt="" />
            <h2>Address:</h2>
            <p>Block 1, dash dash street, Toronton, Canada</p>

            <h2>Contact:</h2>
            <ul>
              <li>
                <a href="">09055380387</a>
              </li>
              <li>
                <a href="">info@tidostravels.com</a>
              </li>
            </ul>
            <ul className="socm">
              <li>
                <a href="">
                  <img src="/images/soc1.svg" alt="" />
                </a>
              </li>
              <li>
                <a href="">
                  <img src="/images/soc2.svg" alt="" />
                </a>
              </li>
              <li>
                <a href="">
                  <img src="/images/soc3.svg" alt="" />
                </a>
              </li>
              <li>
                <a href="">
                  <img src="/images/soc4.svg" alt="" />
                </a>
              </li>
            </ul>
          </div>
          <div className="col2">
            <h3>Column One</h3>
            <ul className="links">
              <li>
                <a href="">Link One</a>
              </li>
              <li>
                <a href="">Link Two</a>
              </li>
              <li>
                <a href="">Link Three</a>
              </li>
              <li>
                <a href="">Link Four</a>
              </li>
              <li>
                <a href="">Link Five</a>
              </li>
            </ul>
          </div>
          <div className="col3">
            <h3>Column Two</h3>
            <ul className="links">
              <li>
                <a href="">Link Six</a>
              </li>
              <li>
                <a href="">Link Seven</a>
              </li>
              <li>
                <a href="">Link Eight</a>
              </li>
              <li>
                <a href="">Link Nine</a>
              </li>
              <li>
                <a href="">Link Ten</a>
              </li>
            </ul>
          </div>
          <div className="col4">
            <h3>Column Three</h3>
            <ul className="links">
              <li>
                <a href="">Link Eleven</a>
              </li>
              <li>
                <a href="">Link Twelve</a>
              </li>
              <li>
                <a href="">Link Thirteen</a>
              </li>
              <li>
                <a href="">Link Fourteen</a>
              </li>
              <li>
                <a href="">Link Fifteen</a>
              </li>
            </ul>
          </div>
        </div>
        <div className="lastline">
          <p>2022 Tidos Travels. All right reserved.</p>
          <ul>
            <li>
              <a href="">Privacy Policy</a>
            </li>
            <li>
              <a href="">Terms of Service</a>
            </li>
            <li>
              <a href="">Cookies Settings</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
