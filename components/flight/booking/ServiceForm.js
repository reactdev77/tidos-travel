export default function ServiceForm() {
  return (
    <div className="service-details">
      <div className="service_title">
        <h2 style={{ fontWeight: "bold" }}>Add baggage</h2>
      </div>
      <div className="service-item">
        <p>Carry on baggage</p>
        <select>
          <option value=""></option>
          <option value=""></option>
        </select>
      </div>
      <div className="service-item">
        <p>Checked baggage</p>
        <select>
          <option value=""></option>
          <option value=""></option>
        </select>
      </div>
    </div>
  );
}
