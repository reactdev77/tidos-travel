export default function UserForm() {
  return (
    <div className="user-details">
      <div className="user_title">
        <h3>Traveller</h3>
      </div>
      <div className="user-details-item">
        <p>Title</p>
        <select>
          <option value="">Mr</option>
          <option value="">Miss</option>
        </select>
      </div>
      <div className="user-details-item">
        <p>Full Name</p>
        <input type="text" />
      </div>
      <div className="user-details-item">
        <p>Email</p>
        <input type="password" />
      </div>
      <div className="user-details-item">
        <p>Phone Number</p>
        <input type="text" />
      </div>
      <div className="user-details-item">
        <p>Birthday</p>
        <input type="text" />
      </div>
      <div className="user-details-item">
        <p>Gender</p>
        <select>
          <option value="">Male</option>
          <option value="">Female</option>
        </select>
      </div>
      <div className="user-details-item">
        <p>Gift Code</p>
        <input type="text" />
      </div>
      <div className="user-details-item">
        <p>Document Type</p>
        <select>
          <option value="">Identity Card</option>
          <option value="">Passport</option>
        </select>
      </div>
      <div className="user-details-item">
        <p>Issued by</p>
        <input type="text" />
      </div>
      <div className="user-details-item">
        <p>Passport Number</p>
        <input type="text" />
      </div>
      <div className="user-details-item">
        <p>Passport Expiration</p>
        <input type="text" />
      </div>
      <div className="user-details-item">
        <p>Postcode of residence</p>
        <input type="text" />
      </div>
    </div>
  );
}
