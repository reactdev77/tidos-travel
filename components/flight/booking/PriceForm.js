export default function PriceForm() {
  return (
    <div>
      <div className="flight-details">
        <div className="flight-card">
          <div className="lightfrom_to">
            <h2>Los Angeles</h2>
            <h1>SFO</h1>
            <p>Apr 7</p>
            <p>2:00 PM</p>
          </div>
          <div className="durations">
            <img src="/images/duration.svg" alt="" />
            <p>22 hours long</p>
            <p className="redp">2 stops</p>
          </div>
          <div className="lightfrom_to">
            <h2>New York City</h2>
            <h1>NYC</h1>
            <p>Apr 9</p>
            <p>6:00 PM</p>
          </div>
        </div>
        <div className="economy">
          <p>Economy Class</p>
          <p>UA 902Y</p>
        </div>
      </div>

      <div className="price-details">
        <h4>Price details</h4>
        <div>
          <h5 style={{ opacity: 0.8 }}>1 room x 1 night</h5>
          <div style={{ textAlign: "right" }}>
            <p>$150</p>
            <p style={{ fontSize: 10 }}>2 TDO</p>
          </div>
        </div>
        <div>
          <h5 style={{ opacity: 0.8 }}>Taxes and fees</h5>
          <div style={{ textAlign: "right" }}>
            <p>$22.4</p>
            <p style={{ fontSize: 10 }}>0.2 TDO</p>
          </div>
        </div>
        <div style={{ marginTop: 40 }}>
          <p style={{ fontWeight: "700" }}>Total</p>
          <div style={{ textAlign: "right" }}>
            <p style={{ fontWeight: "700", color: "#5452b5" }}>$172.4</p>
            <p style={{ fontSize: 12, color: "#5452b5" }}>2.2 TDO</p>
          </div>
        </div>
      </div>
    </div>
  );
}
