import $ from "jquery";
import Router from "next/router";
import { useState } from "react";
import DatePicker from "react-datepicker";
import Trip from "../../common/Trip";

let nextDay = new Date();
nextDay.setDate(nextDay.getDate() + 1);

export default function FlightSearchResult() {
  const [flightDate, setFlightDate] = useState(new Date());
  const [flightDateRange, setFlightDateRange] = useState([new Date(), nextDay]);
  const [flightStartDate, flightEndDate] = flightDateRange;
  const [trip, setTrip] = useState("double");

  const showFilter = () => {
    $(".popuponphone_rightside").addClass("rightside-active");
    $("body").addClass("disable_scroll");
  };

  const closeFilter = () => {
    $(".popuponphone_rightside").removeClass("rightside-active");
    $("body").removeClass("disable_scroll");
  };

  const handleMobileSearch = () => {
    $(".search_bar").addClass("mobile-view");
    $(".search_bar").removeClass("mobile");
  };

  const handleCloseModal = () => {
    $(".search_bar").removeClass("mobile-view");
    $(".search_bar").addClass("mobile");
  };
  return (
    <>
      <div className="search-result_flight">
        <div className="areaforsearch" onClick={() => handleMobileSearch()}>
          <h2>Los Angeles to New York City </h2>
          <p>Thu, Apr 7 - Fri, Apr 9 - 1 traveller| one way</p>
          <img src="/images/search_icon.svg" className="search_icon" alt="" />
        </div>

        <div className="search_bar search_bar_alltimevisible search_bar_desktop">
          <Trip {...{ trip, setTrip }} />
          <div className="box inputs_flight">
            <div className="inputs">
              <input
                type="text"
                className="classic-input location-input"
                placeholder="FLYING FROM"
              />
              <input
                type="text"
                className="classic-input location-input"
                placeholder="FLYING TO"
              />
              {trip === "single" ? (
                <div>
                  <DatePicker
                    wrapperClassName="classic-datepicker calendar-icon"
                    selected={flightDate}
                    onChange={(date) => setFlightDate(date)}
                    minDate={new Date()}
                    showDisabledMonthNavigation
                    shouldCloseOnSelect={false}
                  />
                </div>
              ) : (
                <div>
                  <DatePicker
                    wrapperClassName="classic-datepicker calendar-icon"
                    selectsRange={true}
                    minDate={new Date()}
                    startDate={flightStartDate}
                    endDate={flightEndDate}
                    onChange={(update) => {
                      setFlightDateRange(update);
                    }}
                    monthsShown={2}
                  />
                </div>
              )}
              <select name="" id="" className="classic-select flight_select">
                <option value="">ECONOMY - 1 PASSENGER</option>
                <option value="">Options</option>
              </select>
            </div>
            <button className="search_button">SEARCH</button>
          </div>
        </div>
        <div className="firstarea">
          <h2 className="bestplaces">We’ve the best places just for you </h2>
          <h3 className="bestplaces_desc">
            <span>30</span>{" "}
            <span className="op">fantastic places found in</span>{" "}
            <span>Toronto, Canada</span>
          </h3>

          <a onClick={showFilter} className="phonefilter">
            <img src="/images/filters.svg" alt="" />
          </a>
        </div>

        <div className="filters_s_r">
          <div className="leftside">
            <select className="select" name="" id="">
              <option value="">Price</option>
              <option value="">$150</option>
              <option value="">$250</option>
              <option value="">$350</option>
            </select>
            <select className="select" name="" id="">
              <option value="">STOPS</option>
              <option value="">Option1</option>
              <option value="">Option2</option>
              <option value="">Option3</option>
            </select>
            <select className="select" name="" id="">
              <option value="">AIRLINES</option>
              <option value="">Option1</option>
              <option value="">Option2</option>
              <option value="">Option3</option>
            </select>
            <a href="" className="morefilteroptions">
              MORE FILTER OPTIONS
            </a>
          </div>
        </div>

        <div className="result_boxes">
          <div className="rightside popuponphone_rightside">
            <h2 className="fiterotpions_title">
              More filter Options
              <a onClick={closeFilter} className="xicon">
                <img src="/images/xicon1.svg" alt="" />
              </a>
            </h2>
            <div className="content">
              <div className="price">
                <h2 className="fiterotpions_subtitle">Price </h2>
                <div className="rangeslider">
                  <div className="conc">
                    <div className="dots">
                      <div className="leftc" style={{ left: "24%" }}></div>
                      <div className="rightc" style={{ left: "100%" }}></div>
                      <div
                        className="line"
                        style={{
                          left: "calc(24% - 5px)",
                          width: "calc(76% - 5px)",
                        }}
                      ></div>
                    </div>
                  </div>
                  <div className="values">
                    <h2>$89</h2>
                    <h2>$400</h2>
                  </div>
                </div>
              </div>
              <div className="stops">
                <h2 className="fiterotpions_subtitle">Stops</h2>
                <div className="radiobtn">
                  <input type="radio" id="radio1" name="radio-group" readOnly />
                  <label htmlFor="radio1">Any amount of stops</label>
                </div>
                <div className="radiobtn">
                  <input type="radio" id="radio2" name="radio-group" />
                  <label htmlFor="radio2">Non stop</label>
                </div>
                <div className="radiobtn">
                  <input type="radio" id="radio3" name="radio-group" />
                  <label htmlFor="radio3">1 stop or fewer</label>
                </div>
                <div className="radiobtn">
                  <input type="radio" id="radio4" name="radio-group" />
                  <label htmlFor="radio4">2 stops or fewer</label>
                </div>
              </div>
              <div className="airlines">
                <h2 className="fiterotpions_subtitle">Airlines</h2>
                <h2 className="selectall">
                  Select all airlines
                  <label className="container">
                    <input type="checkbox" readOnly="readOnly" />
                    <span className="checkmark"></span>
                  </label>
                </h2>

                <div className="tworow">
                  <div className="box">
                    <div className="customcheckbox_v2">
                      <label className="container_cb">
                        Air Baltic
                        <input type="checkbox" readOnly />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                    <div className="customcheckbox_v2">
                      <label className="container_cb">
                        Air Europa
                        <input type="checkbox" readOnly />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                    <div className="customcheckbox_v2">
                      <label className="container_cb">
                        Air Baltic
                        <input type="checkbox" readOnly />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                    <div className="customcheckbox_v2">
                      <label className="container_cb">
                        Air Europa
                        <input type="checkbox" readOnly />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                    <div className="customcheckbox_v2">
                      <label className="container_cb">
                        Air Baltic
                        <input type="checkbox" readOnly />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                    <div className="customcheckbox_v2">
                      <label className="container_cb">
                        Air Europa
                        <input type="checkbox" readOnly />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                  </div>
                  <div className="box">
                    <div className="customcheckbox_v2">
                      <label className="container_cb">
                        Air France
                        <input type="checkbox" readOnly />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                    <div className="customcheckbox_v2">
                      <label className="container_cb">
                        Air France
                        <input type="checkbox" readOnly />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                    <div className="customcheckbox_v2">
                      <label className="container_cb">
                        Air France
                        <input type="checkbox" readOnly />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                    <div className="customcheckbox_v2">
                      <label className="container_cb">
                        Air France
                        <input type="checkbox" readOnly />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                    <div className="customcheckbox_v2">
                      <label className="container_cb">
                        Air France
                        <input type="checkbox" readOnly />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                    <div className="customcheckbox_v2">
                      <label className="container_cb">
                        Air France
                        <input type="checkbox" readOnly />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div className="times">
                <h2 className="fiterotpions_subtitle">Times</h2>

                <div className="rangeslider">
                  <div className="values_v2">
                    <div>
                      <img src="/images/time_icon1.svg" alt="" />
                      <p>Departure time</p>
                    </div>
                    <div>
                      <h2>08:45 - 23:59</h2>
                    </div>
                  </div>
                  <div className="conc">
                    <div className="dots">
                      <div className="leftc" style={{ left: "24%" }}></div>
                      <div className="rightc" style={{ left: "100%" }}></div>
                      <div
                        className="line"
                        style={{
                          left: "calc(24% - 5px)",
                          width: "calc(76% - 5px)",
                        }}
                      ></div>
                    </div>
                  </div>
                </div>
                <div className="rangeslider">
                  <div className="values_v2">
                    <div>
                      <img src="/images/time_icon1.svg" alt="" />
                      <p>Arrival time</p>
                    </div>
                    <div>
                      <h2>08:45 - 23:59</h2>
                    </div>
                  </div>
                  <div className="conc">
                    <div className="dots">
                      <div className="leftc" style={{ left: "24%" }}></div>
                      <div className="rightc" style={{ left: "100%" }}></div>
                      <div
                        className="line"
                        style={{
                          left: "calc(24% - 5px)",
                          width: "calc(76% - 5px)",
                        }}
                      ></div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="times">
                <h2 className="fiterotpions_subtitle">Duration</h2>

                <div className="rangeslider">
                  <div className="values_v2">
                    <div>
                      <img src="/images/duration_clock.svg" alt="" />
                      <p>Departure time</p>
                    </div>
                    <div>
                      <h2>3:45hrs - 22:40hrs</h2>
                    </div>
                  </div>
                  <div className="conc">
                    <div className="dots">
                      <div className="leftc" style={{ left: "24%" }}></div>
                      <div className="rightc" style={{ left: "100%" }}></div>
                      <div
                        className="line"
                        style={{
                          left: "calc(24% - 5px)",
                          width: "calc(76% - 5px)",
                        }}
                      ></div>
                    </div>
                  </div>
                </div>
              </div>
              <button className="applyfilter">Apply Filter</button>
            </div>
          </div>
          <div className="leftside">
            <div className="flight-card">
              <div className="aviocompany">
                <img src="/images/avio_company.png" alt="" />
                <p>Egypt Air</p>
              </div>
              <div className="lightfrom_to">
                <h2>Los Angeles</h2>
                <h1>SFO</h1>
                <p>Apr 7</p>
                <p>2:00 PM</p>
              </div>
              <div className="durations">
                <img src="/images/duration.svg" alt="" />
                <p>22 hours long</p>
                <p className="redp">2 stops</p>
              </div>
              <div className="lightfrom_to">
                <h2>New York City</h2>
                <h1>NYC</h1>
                <p>Apr 9</p>
                <p>6:00 PM</p>
              </div>
              <div className="bookflight">
                <h2>$568.00</h2>
                <a
                  onClick={() => Router.push("/flights/booking")}
                  className="defbtn"
                >
                  Book flight
                </a>
              </div>
            </div>
            <div className="flight-card">
              <div className="aviocompany">
                <img src="/images/avio_company.png" alt="" />
                <p>Egypt Air</p>
              </div>
              <div className="lightfrom_to">
                <h2>Los Angeles</h2>
                <h1>SFO</h1>
                <p>Apr 7</p>
                <p>2:00 PM</p>
              </div>
              <div className="durations">
                <img src="/images/duration.svg" alt="" />
                <p>22 hours long</p>
                <p className="redp">2 stops</p>
              </div>
              <div className="lightfrom_to">
                <h2>New York City</h2>
                <h1>NYC</h1>
                <p>Apr 9</p>
                <p>6:00 PM</p>
              </div>
              <div className="bookflight">
                <h2>$568.00</h2>
                <a
                  onClick={() => Router.push("/flights/booking")}
                  className="defbtn"
                >
                  Book flight
                </a>
              </div>
            </div>
            <div
              className="flight-card"
              onClick={() => Router.push("/flights/booking")}
            >
              <div className="aviocompany">
                <img src="/images/avio_company.png" alt="" />
                <p>Egypt Air</p>
              </div>
              <div className="lightfrom_to">
                <h2>Los Angeles</h2>
                <h1>SFO</h1>
                <p>Apr 7</p>
                <p>2:00 PM</p>
              </div>
              <div className="durations">
                <img src="/images/duration.svg" alt="" />
                <p>22 hours long</p>
                <p className="redp">2 stops</p>
              </div>
              <div className="lightfrom_to">
                <h2>New York City</h2>
                <h1>NYC</h1>
                <p>Apr 9</p>
                <p>6:00 PM</p>
              </div>
              <div className="bookflight">
                <h2>$568.00</h2>
                <a
                  onClick={() => Router.push("/flights/booking")}
                  className="defbtn"
                >
                  Book flight
                </a>
              </div>
            </div>
            <div
              className="flight-card"
              onClick={() => Router.push("/flights/booking")}
            >
              <div className="aviocompany">
                <img src="/images/avio_company.png" alt="" />
                <p>Egypt Air</p>
              </div>
              <div className="lightfrom_to">
                <h2>Los Angeles</h2>
                <h1>SFO</h1>
                <p>Apr 7</p>
                <p>2:00 PM</p>
              </div>
              <div className="durations">
                <img src="/images/duration.svg" alt="" />
                <p>22 hours long</p>
                <p className="redp">2 stops</p>
              </div>
              <div className="lightfrom_to">
                <h2>New York City</h2>
                <h1>NYC</h1>
                <p>Apr 9</p>
                <p>6:00 PM</p>
              </div>
              <div className="bookflight">
                <h2>$568.00</h2>
                <a
                  onClick={() => Router.push("/flights/booking")}
                  className="defbtn"
                >
                  Book flight
                </a>
              </div>
            </div>
            <div
              className="flight-card"
              onClick={() => Router.push("/flights/booking")}
            >
              <div className="aviocompany">
                <img src="/images/avio_company.png" alt="" />
                <p>Egypt Air</p>
              </div>
              <div className="lightfrom_to">
                <h2>Los Angeles</h2>
                <h1>SFO</h1>
                <p>Apr 7</p>
                <p>2:00 PM</p>
              </div>
              <div className="durations">
                <img src="/images/duration.svg" alt="" />
                <p>22 hours long</p>
                <p className="redp">2 stops</p>
              </div>
              <div className="lightfrom_to">
                <h2>New York City</h2>
                <h1>NYC</h1>
                <p>Apr 9</p>
                <p>6:00 PM</p>
              </div>
              <div className="bookflight">
                <h2>$568.00</h2>
                <a
                  onClick={() => Router.push("/flights/booking")}
                  className="defbtn"
                >
                  Book flight
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="search_bar mobile search_disabledondesktop">
        <a onClick={handleCloseModal} className="xicon"></a>

        <div className="box inputs_flight">
          <Trip {...{ trip, setTrip }} />
          <div className="inputs">
            <input
              type="text"
              className="classic-input location-input"
              placeholder="FLYING FROM"
            />
            <input
              type="text"
              className="classic-input location-input"
              placeholder="FLYING TO"
            />
            {trip === "single" ? (
              <DatePicker
                wrapperClassName="classic-datepicker calendar-icon"
                selected={flightDate}
                onChange={(date) => setFlightDate(date)}
                minDate={new Date()}
                showDisabledMonthNavigation
                shouldCloseOnSelect={false}
              />
            ) : (
              <DatePicker
                wrapperClassName="classic-datepicker calendar-icon"
                selectsRange={true}
                minDate={new Date()}
                startDate={flightStartDate}
                endDate={flightEndDate}
                onChange={(update) => {
                  setFlightDateRange(update);
                }}
                monthsShown={2}
              />
            )}
            <select name="" id="" className="classic-select flight_select">
              <option value="">ECONOMY - 1 PASSENGER</option>
              <option value="">Options</option>
            </select>
          </div>
          <button className="search_button">SEARCH</button>
        </div>
      </div>
    </>
  );
}
