import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Link from "next/link";
import $ from "jquery";
import { useDispatch } from "react-redux";

import { loginAction } from "../../store/actions";
import Spinner from "../common/Spinner";
import styles from "./Account.module.css";

const Login = ({ setPage, close }) => {
  const dispatch = useDispatch();
  const [success, setSuccess] = useState(false);
  const [loading, setLoading] = useState(false);
  const [hidden, setHidden] = useState(true);

  useEffect(() => {
    if (success) close();
    if ($(".header").hasClass("header_active")) {
      $(".header").removeClass("header_active");
      $("body").removeClass("disable_scroll");
    }
  });

  // form validation rules
  const validationOptions = {
    email: { required: "Email is required" },
    password: {
      required: "Password is required",
      minLength: {
        value: 8,
        message: "Password must have at least 8 characters",
      },
    },
  };

  // get functions to build form with useForm() hook
  const { register, handleSubmit, formState } = useForm();
  const { errors } = formState;

  async function onSubmit({ email, password }) {
    setLoading(true);
    const result = await dispatch(loginAction(email, password));
    setLoading(false);
    if (result) {
      setSuccess(true);
    }
  }

  return (
    <>
      <div className={styles.login}>
        <div className={styles.box}>
          <div className={styles.leftside}>
            <h2>Login</h2>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className={styles.card}>
                <p>Email </p>
                <input
                  className={errors.email?.message && styles.error}
                  name="email"
                  type="text"
                  {...register("email", validationOptions.email)}
                />
                {/* <p className={styles.error}>{errors.email?.message}</p> */}
              </div>
              <div className={styles.card}>
                <p>Password</p>
                <input
                  name="password"
                  type={hidden ? "password" : "text"}
                  {...register("password", validationOptions.password)}
                  className={errors.password?.message && styles.error}
                />
                <div style={{ textAlign: "right", marginTop: 5 }}>
                  <Link href="/account/forgot-password">
                    <a style={{ color: "#5452b5" }}>Forgot Password?</a>
                  </Link>
                </div>
              </div>
              <button disabled={loading}>
                {loading ? <Spinner loading={loading} /> : "Login"}
              </button>
            </form>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                marginTop: 20,
              }}
            >
              <p>
                {`Don't have an account? `}
                <a
                  style={{ cursor: "pointer", color: "#5452b5" }}
                  onClick={() => setPage("signup")}
                >
                  Sign Up
                </a>
              </p>
            </div>
          </div>
          <div className={styles.rightside}>
            <div className={styles.cont}>
              <h1>The World awaits you.</h1>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
