import { useForm } from "react-hook-form";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Web3 from "web3";
import $ from "jquery";

import { signupAction } from "../../store/actions";
import Spinner from "../common/Spinner";
import styles from "./Account.module.css";

const Register = ({ setPage }) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [isSignedUp, setIsSignedUp] = useState(false);
  const [hidden, setHidden] = useState(true);

  useEffect(() => {
    if (isSignedUp) setPage("login");
    if ($(".header").hasClass("header_active")) {
      $(".header").removeClass("header_active");
      $("body").removeClass("disable_scroll");
    }
  }, [isSignedUp, setPage]);

  // form validation rules
  const validationOptions = {
    name: { required: "Name is required" },
    email: { required: "Email is required" },
    // secret: {
    //   required: "Security key is required",
    //   minLength: {
    //     value: 8,
    //     message: "At least 8 characters",
    //   },
    // },
    password: {
      required: "Password is required",
      minLength: {
        value: 8,
        message: "At least 8 characters",
      },
    },
  };

  // get functions to build form with useForm() hook
  const { register, handleSubmit, formState } = useForm();
  const { errors } = formState;

  async function onSubmit(user) {
    setLoading(true);
    // const testBSC_URL = "https://data-seed-prebsc-1-s1.binance.org:8545";
    const mainBSC_URL = "https://bsc-dataseed1.binance.org:443";
    const web3 = new Web3(mainBSC_URL);
    const account = web3.eth.accounts.create();
    const encryptedKey = web3.eth.accounts.encrypt(
      account.address,
      user.secret
    );
    user["address"] = account.address;
    // user["prikey"] = encryptedKey;
    user["prikey"] = "private-encrypted-key";
    user["encryptedKey"] = "";
    const result = await dispatch(signupAction(user));
    setLoading(false);
    if (result) {
      setIsSignedUp(true);
    } else {
    }
  }

  return (
    <>
      <div className={styles.login}>
        <div className={styles.box}>
          <div className={styles.leftside}>
            <h2>Sign up</h2>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className={styles.card}>
                <p>Full Name </p>
                <input
                  className={errors.name?.message && styles.error}
                  name="name"
                  type="text"
                  {...register("name", validationOptions.name)}
                />
                {/* <p className={styles.error}>{errors.name?.message}</p> */}
              </div>
              <div className={styles.card}>
                <p>Email </p>
                <input
                  className={errors.email?.message && styles.error}
                  name="email"
                  type="text"
                  {...register("email", validationOptions.email)}
                />
                {/* <p className={styles.error}>{errors.email?.message}</p> */}
              </div>
              {/* <div className={styles.card}>
                <p>Security Key </p>
                <input
                  className={errors.secret?.message && styles.error}
                  name="secret"
                  type="password"
                  {...register("secret", validationOptions.secret)}
                />
                <p className={styles.error}>{errors.secret?.message}</p>
              </div> */}
              <div className={styles.card}>
                <p>Password</p>
                <input
                  className={errors.password?.message && styles.error}
                  name="password"
                  type={hidden ? "password" : "text"}
                  {...register("password", validationOptions.password)}
                />
                <p className={styles.error}>{errors.password?.message}</p>
              </div>
              <button disabled={loading}>
                {loading ? <Spinner loading={loading} /> : "Sign up"}
              </button>
            </form>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                marginTop: 20,
              }}
            >
              <p>
                {`Already have an account? `}
                <a
                  style={{ cursor: "pointer", color: "#5452b5" }}
                  onClick={() => setPage("login")}
                >
                  Login
                </a>
              </p>
            </div>
          </div>
          <div className={styles.rightside}>
            <div className={styles.cont}>
              <h1>The World awaits you.</h1>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Register;
