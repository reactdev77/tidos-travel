import $ from "jquery";

export default function Sort({
  viewMode,
  setViewMode,
  visibleFilter,
  setVisibleFilter,
}) {
  const handleFilter = () => {
    if (!visibleFilter) setVisibleFilter(true);
  };
  return (
    <div className="filters_s_r">
      <div className="leftside">
        <select className="select" name="" id="">
          <option value="">Price</option>
          <option value="">$150</option>
          <option value="">$250</option>
          <option value="">$350</option>
        </select>
        <select className="select" name="" id="">
          <option value="">Rating</option>
          <option value="">Option1</option>
          <option value="">Option2</option>
          <option value="">Option3</option>
        </select>
        <select className="select" name="" id="">
          <option value="">Top Rated</option>
          <option value="">Option1</option>
          <option value="">Option2</option>
          <option value="">Option3</option>
        </select>
        <a className="morefilteroptions" onClick={handleFilter}>
          MORE FILTER OPTIONS
        </a>
      </div>
      <div className="rightside">
        <a
          onClick={() => setViewMode("grid")}
          className={`filter_options ${
            viewMode === "grid" ? "filter_options_active" : undefined
          }`}
        >
          <svg
            width="20"
            height="18"
            viewBox="0 0 20 18"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M20 9.999V17C20 17.2652 19.8946 17.5196 19.7071 17.7071C19.5196 17.8946 19.2652 18 19 18H11V9.999H20ZM9 9.999V18H1C0.734784 18 0.48043 17.8946 0.292893 17.7071C0.105357 17.5196 0 17.2652 0 17V9.999H9ZM9 0V7.999H0V1C0 0.734784 0.105357 0.48043 0.292893 0.292893C0.48043 0.105357 0.734784 0 1 0H9ZM19 0C19.2652 0 19.5196 0.105357 19.7071 0.292893C19.8946 0.48043 20 0.734784 20 1V7.999H11V0H19Z"
              className="fill"
            />
          </svg>
        </a>
        <a
          onClick={() => setViewMode("list")}
          className={`filter_options ${
            viewMode === "list" ? "filter_options_active" : undefined
          }`}
        >
          <svg
            width="18"
            height="18"
            viewBox="0 0 18 18"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M5 1H18V3H5V1ZM1.5 3.5C1.10218 3.5 0.720644 3.34196 0.43934 3.06066C0.158035 2.77936 0 2.39782 0 2C0 1.60218 0.158035 1.22064 0.43934 0.93934C0.720644 0.658035 1.10218 0.5 1.5 0.5C1.89782 0.5 2.27936 0.658035 2.56066 0.93934C2.84196 1.22064 3 1.60218 3 2C3 2.39782 2.84196 2.77936 2.56066 3.06066C2.27936 3.34196 1.89782 3.5 1.5 3.5ZM1.5 10.5C1.10218 10.5 0.720644 10.342 0.43934 10.0607C0.158035 9.77936 0 9.39782 0 9C0 8.60218 0.158035 8.22064 0.43934 7.93934C0.720644 7.65804 1.10218 7.5 1.5 7.5C1.89782 7.5 2.27936 7.65804 2.56066 7.93934C2.84196 8.22064 3 8.60218 3 9C3 9.39782 2.84196 9.77936 2.56066 10.0607C2.27936 10.342 1.89782 10.5 1.5 10.5ZM1.5 17.4C1.10218 17.4 0.720644 17.242 0.43934 16.9607C0.158035 16.6794 0 16.2978 0 15.9C0 15.5022 0.158035 15.1206 0.43934 14.8393C0.720644 14.558 1.10218 14.4 1.5 14.4C1.89782 14.4 2.27936 14.558 2.56066 14.8393C2.84196 15.1206 3 15.5022 3 15.9C3 16.2978 2.84196 16.6794 2.56066 16.9607C2.27936 17.242 1.89782 17.4 1.5 17.4ZM5 8H18V10H5V8ZM5 15H18V17H5V15Z"
              className="fill"
            />
          </svg>
        </a>
        <a className="filter_options filter_options_map">
          <svg
            width="12"
            height="15"
            viewBox="0 0 12 15"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M10.2427 10.576L6 14.8187L1.75734 10.576C0.918228 9.73687 0.346791 8.66777 0.115286 7.50389C-0.11622 6.34 0.00260456 5.13361 0.456732 4.03726C0.91086 2.9409 1.6799 2.00384 2.66659 1.34455C3.65328 0.685266 4.81332 0.333374 6 0.333374C7.18669 0.333374 8.34672 0.685266 9.33342 1.34455C10.3201 2.00384 11.0891 2.9409 11.5433 4.03726C11.9974 5.13361 12.1162 6.34 11.8847 7.50389C11.6532 8.66777 11.0818 9.73687 10.2427 10.576ZM6 7.66665C6.35362 7.66665 6.69276 7.52618 6.94281 7.27613C7.19286 7.02608 7.33334 6.68694 7.33334 6.33332C7.33334 5.9797 7.19286 5.64056 6.94281 5.39051C6.69276 5.14046 6.35362 4.99999 6 4.99999C5.64638 4.99999 5.30724 5.14046 5.05719 5.39051C4.80715 5.64056 4.66667 5.9797 4.66667 6.33332C4.66667 6.68694 4.80715 7.02608 5.05719 7.27613C5.30724 7.52618 5.64638 7.66665 6 7.66665Z"
              fill="#5452B5"
            />
          </svg>
          <span>MAP</span>
        </a>
      </div>
    </div>
  );
}
