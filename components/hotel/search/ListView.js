export default function ListView({ visibleFilter, children }) {
  return (
    <div
      className={`flightboxes listofcard_list ${
        visibleFilter ? "filtersrightopened" : undefined
      }`}
    >
      {children}
      <div className="leftside box">
        <div className="card_result">
          <img src="/images/bg_card.png" className="imageforcard" alt="" />
          <h1>Herta Berlin Hotel</h1>
          <div className="location_rew">
            <div className="l">
              <img src="/images/location_icon.svg" alt="" />
              <p>SCOTLAND</p>
            </div>
            <div className="r">
              <img src="/images/Star.svg" alt="" />
              <p>4.8(400 reviews)</p>
            </div>
          </div>
          <p className="desc">
            Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius
            prodesset pri. Veniam delectus ei
          </p>
          <div className="price">
            <a href="">See Map</a>
            <p>
              $568.00 | <span>Avg/Night</span>
            </p>
          </div>
          <a href="" className="viewdetails">
            View Details
          </a>
        </div>
        <div className="card_result">
          <img src="/images/bg_card.png" className="imageforcard" alt="" />
          <h1>Herta Berlin Hotel</h1>
          <div className="location_rew">
            <div className="l">
              <img src="/images/location_icon.svg" alt="" />
              <p>SCOTLAND</p>
            </div>
            <div className="r">
              <img src="/images/Star.svg" alt="" />
              <p>4.8(400 reviews)</p>
            </div>
          </div>
          <p className="desc">
            Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius
            prodesset pri. Veniam delectus ei
          </p>
          <div className="price">
            <a href="">See Map</a>
            <p>
              $568.00 | <span>Avg/Night</span>
            </p>
          </div>
          <a href="" className="viewdetails">
            View Details
          </a>
        </div>
      </div>
    </div>
  );
}
