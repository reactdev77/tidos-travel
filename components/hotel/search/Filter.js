import $ from "jquery";

export default function Filter({
  visibleMobileFilter,
  setVisibleFilter,
  setVisibleMobileFilter,
}) {
  const handleClose = () => {
    visibleMobileFilter
      ? setVisibleMobileFilter(false)
      : setVisibleFilter(false);
  };
  return (
    <div
      className={`rightside popuponphone_rightside ${
        visibleMobileFilter ? "rightside-active" : undefined
      }`}
    >
      <h2 className="fiterotpions_title">
        Refine your search
        <a onClick={() => handleClose(false)} className="xicon">
          <img src="/images/xicon.svg" alt="close icon" />
        </a>
      </h2>
      <div className="content">
        <div className="price">
          <h2 className="fiterotpions_subtitle">Price </h2>
          <div className="rangeslider">
            <div className="conc">
              <div className="dots">
                <div className="leftc" style={{ left: "24%" }}></div>
                <div className="rightc" style={{ left: "100%" }}></div>
                <div
                  className="line"
                  style={{
                    left: "calc(24% - 5px)",
                    width: "calc(76% - 5px)",
                  }}
                ></div>
              </div>
            </div>
            <div className="values">
              <h2>$89</h2>
              <h2>$400</h2>
            </div>
          </div>
        </div>
        <div className="stars_box">
          <h2 className="fiterotpions_subtitle">Star rating</h2>
          <div className="stars">
            <img src="/images/star_yellow.svg" alt="" />
            <img src="/images/star_yellow.svg" alt="" />
            <img src="/images/star_yellow.svg" alt="" />
            <img src="/images/star_yellow.svg" alt="" />
            <img src="/images/star_gray.svg" alt="" />
          </div>
        </div>
        <div className="paymenttype">
          <h2 className="fiterotpions_subtitle">Payment type</h2>
          <div className="tworow">
            <div className="box">
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Free Cancellation
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div className="roomsp">
          <h2 className="fiterotpions_subtitle">Room Specifics</h2>
          <div className="roomspcifics">
            <div className="optionwithcount">
              Beds
              <div className="count">
                <a href="" className="disabled">
                  <img src="/images/minus.svg" alt="" />
                </a>
                <p className="num">3</p>
                <a href="">
                  <img src="/images/plus.svg" alt="" />
                </a>
              </div>
            </div>
            <div className="optionwithcount">
              Bedrooms
              <div className="count">
                <a href="" className="disabled">
                  <img src="/images/minus.svg" alt="" />
                </a>
                <p className="num">3</p>
                <a href="">
                  <img src="/images/plus.svg" alt="" />
                </a>
              </div>
            </div>
            <div className="optionwithcount">
              Bathrooms
              <div className="count">
                <a href="" className="disabled">
                  <img src="/images/minus.svg" alt="" />
                </a>
                <p className="num">3</p>
                <a href="">
                  <img src="/images/plus.svg" alt="" />
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="amenties">
          <h2 className="fiterotpions_subtitle">Amenities</h2>

          <div className="tworow">
            <div className="box">
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Free Wifi
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Dining services
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Laundry
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
            </div>
            <div className="box">
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Parking Available
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Conceirge
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Spa
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div className="roomtype">
          <h2 className="fiterotpions_subtitle">Room Type</h2>

          <div className="tworow">
            <div className="box">
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Double
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Suite
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
            </div>
            <div className="box">
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Twin
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
            </div>
          </div>
        </div>

        <div className="roomclasses">
          <h2 className="fiterotpions_subtitle">Room Classes</h2>

          <div className="tworow">
            <div className="box">
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Deluxe
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
            </div>
            <div className="box">
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Superior
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
            </div>
          </div>
        </div>

        <div className="roomclasses">
          <h2 className="fiterotpions_subtitle">Popular Filters</h2>
          <div className="tworow">
            <div className="box">
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Breakfast Included
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Vacation Rentals
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
            </div>
            <div className="box">
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Free Cancellation
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
              <div className="customcheckbox_v2">
                <label className="container_cb">
                  Excellent rating
                  <input type="checkbox" readOnly />
                  <span className="checkmark"></span>
                </label>
              </div>
            </div>
          </div>
        </div>

        <button className="applyfilter">Apply Filter</button>
      </div>
    </div>
  );
}
