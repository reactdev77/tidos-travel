import { useState } from "react";
import $ from "jquery";
import DatePicker from "react-datepicker";
import ListView from "./ListView";
import Sort from "./Sort";
import GridView from "./GridView";
import Filter from "./Filter";

let nextDay = new Date();
nextDay.setDate(nextDay.getDate() + 1);

export default function SearchResult() {
  const [viewMode, setViewMode] = useState("grid");
  const [visibleMobileFilter, setVisibleMobileFilter] = useState(false);
  const [visibleFilter, setVisibleFilter] = useState(true);

  const [hotelDateRange, setHotelDateRange] = useState([new Date(), nextDay]);
  const [hotelStartDate, hotelEndDate] = hotelDateRange;

  const handleMobileSearch = () => {
    $(".search_bar").addClass("mobile-view");
    $(".search_bar").removeClass("mobile");
  };

  const handleCloseModal = () => {
    $(".search_bar").removeClass("mobile-view");
    $(".search_bar").addClass("mobile");
  };

  return (
    <>
      <div className="search_result_hotel">
        <div className="areaforsearch" onClick={() => handleMobileSearch()}>
          <h2>Los Angeles to New York City </h2>
          <p>Thu, Apr 7 - Fri, Apr 9 - 1 traveller| one way</p>
          <img src="/images/search_icon.svg" className="search_icon" alt="" />
        </div>
        <div className="search_bar search_bar_alltimevisible search_bar_desktop">
          <div className="box">
            <div className="inputs">
              <input
                type="text"
                className="classic-input location-input"
                placeholder="LOCATION"
              />
              <DatePicker
                wrapperClassName="classic-datepicker calendar-icon"
                selectsRange={true}
                minDate={new Date()}
                startDate={hotelStartDate}
                endDate={hotelEndDate}
                onChange={(update) => {
                  setHotelDateRange(update);
                }}
                monthsShown={2}
              />

              <select name="" id="" className="classic-select">
                <option value="">ROOM</option>
                <option value="">Options</option>
              </select>
            </div>
            <button className="search_button">SEARCH</button>
          </div>
        </div>
        <div className="firstarea">
          <h2 className="bestplaces">We’ve the best places just for you </h2>

          <div className="filters_phone">
            <a>
              <img src="/images/f1.svg" alt="map img" />
            </a>
            <a>
              <img src="/images/f2.svg" alt="view img" />
            </a>
            <a onClick={() => setVisibleMobileFilter(true)}>
              <img src="/images/f3.svg" alt="filter img" />
            </a>
          </div>
        </div>
        <h3 className="bestplaces_desc">
          <span>30</span> <span className="op">fantastic places found in</span>{" "}
          <span>Toronto, Canada</span>
        </h3>
        <Sort {...{ viewMode, setViewMode, visibleFilter, setVisibleFilter }} />
        {viewMode === "grid" && (
          <GridView {...{ visibleFilter }}>
            {visibleFilter ? (
              <Filter
                {...{
                  visibleMobileFilter,
                  setVisibleFilter,
                  setVisibleMobileFilter,
                }}
              />
            ) : (
              <></>
            )}
          </GridView>
        )}
        {viewMode === "list" && (
          <ListView {...{ visibleFilter }}>
            {visibleFilter ? (
              <Filter
                {...{
                  visibleMobileFilter,
                  setVisibleFilter,
                  setVisibleMobileFilter,
                }}
              />
            ) : (
              <></>
            )}
          </ListView>
        )}
      </div>
      <div className="search_bar mobile search_disabledondesktop">
        <a onClick={handleCloseModal} className="xicon"></a>
        <div className="box">
          <div className="inputs">
            <input
              type="text"
              className="classic-input location-input"
              placeholder="LOCATION"
            />
            <DatePicker
              wrapperClassName="classic-datepicker calendar-icon"
              selectsRange={true}
              minDate={new Date()}
              startDate={hotelStartDate}
              endDate={hotelEndDate}
              onChange={(update) => {
                setHotelDateRange(update);
              }}
              monthsShown={2}
            />

            <select name="" id="" className="classic-select">
              <option value="">ROOM</option>
              <option value="">Options</option>
            </select>
          </div>
          <button className="search_button">SEARCH</button>
        </div>
      </div>
    </>
  );
}
