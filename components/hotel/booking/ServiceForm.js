export default function ServiceForm() {
  return (
    <div className="service-details">
      <div className="service_title">
        <h2 style={{ fontWeight: "bold" }}>Your room details</h2>
      </div>
      <div className="service-item">
        <p>Bed Preference</p>
        <select>
          <option value="">1 king sized bed</option>
          <option value=""></option>
        </select>
      </div>
    </div>
  );
}
