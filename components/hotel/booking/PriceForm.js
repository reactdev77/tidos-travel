export default function PriceForm() {
  return (
    <div>
      <div className="hotel-details">
        <h3>Herta Berlin Hotel</h3>
        <div className="location_rating">
          <div className="hotel_location">
            <img src="/images/location_icon.svg" alt="" />
            <p>scotland</p>
          </div>
          <div className="hotel_rating">
            <img src="/images/Star.svg" alt="" />
            <p>4.8(400 reviews)</p>
          </div>
        </div>
        <div className="hotel_content">
          <p>1 Room: Superior Room,Bathtub</p>
          <p style={{ marginBottom: 20 }}>2 Adults 2 Single Beds Non-smoking</p>
          <div>
            <span>Check_in: </span>
            <span>20/3/2022</span>
          </div>
          <div>
            <span>Check_out: </span>
            <span>30/3/2022</span>
          </div>
          <span>1 night stay</span>
        </div>
      </div>
      <div className="price-details">
        <h4>Price details</h4>
        <div>
          <h5 style={{ opacity: 0.8 }}>1 room x 1 night</h5>
          <div style={{ textAlign: "right" }}>
            <p>$150</p>
            <p style={{ fontSize: 10 }}>2 TDO</p>
          </div>
        </div>
        <div>
          <h5 style={{ opacity: 0.8 }}>Taxes and fees</h5>
          <div style={{ textAlign: "right" }}>
            <p>$22.4</p>
            <p style={{ fontSize: 10 }}>0.2 TDO</p>
          </div>
        </div>
        <div style={{ marginTop: 40 }}>
          <p style={{ fontWeight: "700" }}>Total</p>
          <div style={{ textAlign: "right" }}>
            <p style={{ fontWeight: "700", color: "#5452b5" }}>$172.4</p>
            <p style={{ fontSize: 12, color: "#5452b5" }}>2.2 TDO</p>
          </div>
        </div>
      </div>
    </div>
  );
}
