import Router from "next/router";

export default function Card() {
  return (
    <div className="col-xs-12 col-sm-6 col-md-4">
      <div className="item">
        <div
          className="main-block hotel-block"
          style={{ cursor: "pointer" }}
          onClick={() => Router.push("/hotels/search")}
        >
          <div className="main-img">
            <a>
              <img
                src="/images/destination.png"
                className="img-responsive"
                alt="hotel-img"
                layout="fill"
              />
            </a>
          </div>
          <div className="main-info hotel-info">
            <div className="main-title hotel-title">
              <a>Madrid</a>
              <p>spain</p>
            </div>
            <div>
              <p>
                Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in
                lucilius prodesset pri. Veniam delectus ei
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
