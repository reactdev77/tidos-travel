import BeatLoader from "react-spinners/BeatLoader";

export default function Spinner({ loading }) {
  return <BeatLoader color="white" loading={loading} margin={3} size={12} />;
}
