export default function SearchBar() {
  return (
    <div className="search_bar search_bar_active">
      <a href="" className="xicon">
        <img src="/images/xicon.svg" alt="close icon" />
      </a>
      <div className="box">
        <div className="inputs">
          <input
            type="text"
            className="classic-input location-input"
            placeholder="LOCATION"
          />
          <input
            type="text"
            className="classic-input calendar-icon"
            placeholder="CHECK IN"
          />
          <input
            type="text"
            className="classic-input calendar-icon"
            placeholder="CHECK OUT"
          />
          <select name="" id="" className="classic-select">
            <option value="">ROOM</option>
            <option value="">Options</option>
          </select>
          <select name="" id="" className="classic-select">
            <option value="">ADULT</option>
            <option value="">Options</option>
          </select>
        </div>
        <button>SEARCH</button>
      </div>
    </div>
  );
}
