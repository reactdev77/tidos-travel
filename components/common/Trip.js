export default function Trip({ trip, setTrip }) {
  return (
    <div className="trip_container">
      <div className="trip-item" onClick={() => setTrip("double")}>
        <input
          type="radio"
          value="double"
          checked={trip === "double"}
          readOnly
        />
        <span>Return</span>
      </div>
      <div className="trip-item" onClick={() => setTrip("single")}>
        <input
          type="radio"
          value="single"
          checked={trip === "single"}
          readOnly
        />
        <span>One way</span>
      </div>
    </div>
  );
}
