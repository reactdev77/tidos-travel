import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export default function MainSection({ name, children }) {
  const router = useRouter();
  const [userName, setUserName] = useState("");

  useEffect(() => {
    setUserName(name);
  }, [name]);

  return (
    <section className="innerpage-wrapper">
      <div id="dashboard" className="innerpage-section-padding">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div className="dashboard-heading">
                <h2>
                  <span>Profile</span>
                </h2>
                <p>
                  <span>Hi </span>
                  <span>{userName}</span>
                  <span>, Welcome to Tidos Travels!</span>
                </p>
                <p>
                  All your trips booked with us will appear here and you will be
                  able to manage everything!
                </p>
              </div>

              <div className="dashboard-wrapper">
                <div className="row">
                  <div className="col-xs-12 col-sm-2 col-md-2 dashboard-nav">
                    <ul className="nav nav-tabs nav-stacked text-center">
                      <li
                        className={
                          router.pathname === "/user/dashboard" ? "active" : ""
                        }
                      >
                        <Link href="/user/dashboard">
                          <a>
                            <span>
                              <i className="fa fa-cogs"></i>
                            </span>
                            Dashboard
                          </a>
                        </Link>
                      </li>
                      <li
                        className={
                          router.pathname === "/user/account" ? "active" : ""
                        }
                      >
                        <Link href="/user/account">
                          <a>
                            <span>
                              <i className="fa fa-user"></i>
                            </span>
                            Account
                          </a>
                        </Link>
                      </li>
                      <li
                        className={
                          router.pathname === "/user/booking" ? "active" : ""
                        }
                      >
                        <Link href="/user/booking">
                          <a>
                            <span>
                              <i className="fa fa-briefcase"></i>
                            </span>
                            Booking
                          </a>
                        </Link>
                      </li>
                      <li
                        className={
                          router.pathname === "/user/wallet" ? "active" : ""
                        }
                      >
                        <Link href="/user/wallet">
                          <a>
                            <span>
                              <i className="fa fa-google-wallet"></i>
                            </span>
                            Wallet
                          </a>
                        </Link>
                      </li>
                    </ul>
                  </div>
                  {children}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
