export default function FAQ() {
  return (
    <div className="faq_section">
      <div className="wrapper">
        <h1>Frequently Asked Questions</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
          varius enim in eros elementum tristique.
        </p>

        <div className="twoboxes">
          <div className="box">
            <div className="card">
              <a href="">Question text goes here</a>
              <div className="answer">
                <div className="inside">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse varius enim in eros elementum tristique.
                  </p>
                </div>
              </div>
            </div>
            <div className="card">
              <a href="">Question text goes here</a>
              <div className="answer">
                <div className="inside">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse varius enim in eros elementum tristique.
                  </p>
                </div>
              </div>
            </div>
            <div className="card">
              <a href="">Question text goes here</a>
              <div className="answer">
                <div className="inside">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse varius enim in eros elementum tristique.
                  </p>
                </div>
              </div>
            </div>
            <div className="card">
              <a href="">Question text goes here</a>
              <div className="answer">
                <div className="inside">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse varius enim in eros elementum tristique.
                  </p>
                </div>
              </div>
            </div>
            <div className="card">
              <a href="">Question text goes here</a>
              <div className="answer">
                <div className="inside">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse varius enim in eros elementum tristique.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="box">
            <div className="card">
              <a href="">Question text goes here</a>
              <div className="answer">
                <div className="inside">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse varius enim in eros elementum tristique.
                  </p>
                </div>
              </div>
            </div>
            <div className="card">
              <a href="">Question text goes here</a>
              <div className="answer">
                <div className="inside">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse varius enim in eros elementum tristique.
                  </p>
                </div>
              </div>
            </div>
            <div className="card">
              <a href="">Question text goes here</a>
              <div className="answer">
                <div className="inside">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse varius enim in eros elementum tristique.
                  </p>
                </div>
              </div>
            </div>
            <div className="card">
              <a href="">Question text goes here</a>
              <div className="answer">
                <div className="inside">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse varius enim in eros elementum tristique.
                  </p>
                </div>
              </div>
            </div>
            <div className="card">
              <a href="">Question text goes here</a>
              <div className="answer">
                <div className="inside">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse varius enim in eros elementum tristique.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
