import { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper";

import Card from "../common/Card";
import styles from "./Home.module.css";

export default function Destination() {
  const [target, setTarget] = useState("e");

  const handleTarget = (target) => {
    setTarget(target);
  };

  return (
    <section id="flight-offers" className={styles.destination}>
      <div className="wrapper">
        <div className="row">
          <div className="col-12">
            <div className={`${styles.heading} page-heading`}>
              <h1>Top Destinations</h1>
              <ul className={styles.targets}>
                <li onClick={() => handleTarget("e")}>
                  <a className={target === "e" ? styles.active : undefined}>
                    EUROPE
                  </a>
                </li>
                <li onClick={() => handleTarget("n")}>
                  <a className={target === "n" ? styles.active : undefined}>
                    NORTH AMERICA
                  </a>
                </li>
                <li onClick={() => handleTarget("a")}>
                  <a className={target === "a" ? styles.active : undefined}>
                    ASIA
                  </a>
                </li>
                <li onClick={() => handleTarget("c")}>
                  <a className={target === "c" ? styles.active : undefined}>
                    CENTRAL AMERICAN & CARIBBEAN
                  </a>
                </li>
                <li onClick={() => handleTarget("p")}>
                  <a className={target === "p" ? styles.active : undefined}>
                    PACIFIC OCEAN & AUSTRALIA
                  </a>
                </li>
                <li onClick={() => handleTarget("s")}>
                  <a className={target === "s" ? styles.active : undefined}>
                    SOUTH AMERICA
                  </a>
                </li>
                <li onClick={() => handleTarget("af")}>
                  <a className={target === "af" ? styles.active : undefined}>
                    AFRICA
                  </a>
                </li>
                <li onClick={() => handleTarget("m")}>
                  <a className={target === "m" ? styles.active : undefined}>
                    MIDDLE EAST
                  </a>
                </li>
              </ul>
            </div>
            <Swiper pagination={{ clickable: true }} modules={[Pagination]}>
              <SwiperSlide>
                <div className="row">
                  <Card />
                  <Card />
                  <Card />
                  <Card />
                  <Card />
                  <Card />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="row">
                  <Card />
                  <Card />
                  <Card />
                  <Card />
                  <Card />
                  <Card />
                </div>
              </SwiperSlide>
            </Swiper>
          </div>
        </div>
      </div>
    </section>
  );
}
