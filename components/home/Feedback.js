import { Swiper, SwiperSlide } from "swiper/react";
import { isMobile } from "react-device-detect";

import "swiper/css";
import "swiper/css/pagination";
import { Autoplay } from "swiper";
import styles from "./Home.module.css";

export default function Feedback() {
  return (
    <section className={styles.feedback}>
      <div className="wrapper">
        <div className="row">
          <div className="col-sm-12">
            <div className="page-heading">
              <h1>What our travellers say</h1>
            </div>
          </div>
          <div className="col-sm-12">
            <Swiper
              slidesPerView={isMobile ? 1 : 3}
              spaceBetween={30}
              freeMode={true}
              autoplay={{
                delay: 5000,
                disableOnInteraction: false,
              }}
              breakpoints={{
                340: {
                  slidesPerView: 1,
                },
                640: {
                  slidesPerView: 1,
                },
                768: {
                  slidesPerView: 2,
                  spaceBetween: 40,
                },
                1024: {
                  slidesPerView: 3,
                  spaceBetween: 30,
                },
              }}
              modules={[Autoplay]}
              className="mySwiper"
            >
              <SwiperSlide>
                <CardItem />
              </SwiperSlide>
              <SwiperSlide>
                <CardItem />
              </SwiperSlide>
              <SwiperSlide>
                <CardItem />
              </SwiperSlide>
              <SwiperSlide>
                <CardItem />
              </SwiperSlide>
              <SwiperSlide>
                <CardItem />
              </SwiperSlide>
            </Swiper>
          </div>
        </div>
      </div>
    </section>
  );
}

const CardItem = () => {
  return (
    <div className={styles.carditem}>
      <div className={styles.header}>
        <div className={styles.title}>
          <h4>Jhon Smith</h4>
          <p>Toronto, Canada</p>
        </div>
        <div className={styles.rating}>
          <div className="rating">
            <span>
              <i className="fa fa-star orange"></i>
            </span>
            <span>
              <i className="fa fa-star orange"></i>
            </span>
            <span>
              <i className="fa fa-star orange"></i>
            </span>
            <span>
              <i className="fa fa-star orange"></i>
            </span>
            <span>
              <i className="fa fa-star lightgrey"></i>
            </span>
          </div>
        </div>
      </div>
      <p className={styles.content}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud
      </p>
    </div>
  );
};
