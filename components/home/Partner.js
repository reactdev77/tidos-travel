import styles from "./Home.module.css";

export default function Partner() {
  return (
    <section id="partner" className={styles.partner}>
      <div className="wrapper">
        <div className="row">
          <div className="col-12">
            <div className="page-heading">
              <h1>Our Partners</h1>
              <p>
                Our reliable partners and companies that uses and trusts TIDOS
                Travels
              </p>
            </div>
          </div>
          <img
            className="col-xs-6 col-sm-4 col-md-2"
            src="/images/partner1.png"
            alt="img_partner1"
          />
          <img
            className="col-xs-6 col-sm-4 col-md-2"
            src="/images/partner2.png"
            alt="img_partner2"
          />
          <img
            className="col-xs-6 col-sm-4 col-md-2"
            src="/images/partner3.png"
            alt="img_partner3"
          />
          <img
            className="col-xs-6 col-sm-4 col-md-2"
            src="/images/partner4.png"
            alt="img_partner4"
          />
          <img
            className="col-xs-6 col-sm-4 col-md-2"
            src="/images/partner5.png"
            alt="img_partner5"
          />
          <img
            className="col-xs-6 col-sm-4 col-md-2"
            src="/images/partner6.png"
            alt="img_partner6"
          />
        </div>
      </div>
    </section>
  );
}
