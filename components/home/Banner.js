import { useState } from "react";
import Router, { useRouter } from "next/router";
import DatePicker from "react-datepicker";

import Trip from "../common/Trip";

let nextDay = new Date();
nextDay.setDate(nextDay.getDate() + 1);

const Banner = () => {
  const router = useRouter();
  const [flightDate, setFlightDate] = useState(new Date());
  const [flightDateRange, setFlightDateRange] = useState([new Date(), nextDay]);
  const [flightStartDate, flightEndDate] = flightDateRange;
  const [hotelDateRange, setHotelDateRange] = useState([new Date(), nextDay]);
  const [hotelStartDate, hotelEndDate] = hotelDateRange;

  const [trip, setTrip] = useState("double");

  const handleSearch = () => {
    if (router.pathname === "/") {
      Router.push("/hotels/search");
    } else Router.push("/flights/search");
  };

  return (
    <section className="banner-section">
      <div className="banner">
        <div className="wrapper">
          <div className="box">
            <h1>Discover your Life, travel where you want</h1>
          </div>
        </div>
      </div>

      <div className="search-area">
        <div className="wrapper">
          <div className="additionaloptions search_bar_active">
            <div className="box">
              <a
                onClick={() => Router.push("/")}
                className={router.pathname === "/" ? "active" : ""}
              >
                <svg
                  width="26"
                  height="26"
                  viewBox="0 0 26 26"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_1519_127)">
                    <path
                      d="M17.7555 19.8444H19.8444V11.4889H13.5778V19.8444H15.6666V13.5778H17.7555V19.8444ZM3.13331 19.8444V4.17778C3.13331 3.90077 3.24335 3.63511 3.43922 3.43924C3.63509 3.24337 3.90075 3.13333 4.17776 3.13333H18.8C19.077 3.13333 19.3426 3.24337 19.5385 3.43924C19.7344 3.63511 19.8444 3.90077 19.8444 4.17778V9.4H21.9333V19.8444H22.9778V21.9333H2.08887V19.8444H3.13331ZM7.31109 11.4889V13.5778H9.39998V11.4889H7.31109ZM7.31109 15.6667V17.7556H9.39998V15.6667H7.31109ZM7.31109 7.31111V9.4H9.39998V7.31111H7.31109Z"
                      className="fill"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_1519_127">
                      <rect width="25.0667" height="25.0667" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                Hotels
              </a>
              <a
                onClick={() => Router.push("/flights")}
                className={router.pathname === "/flights" ? "active" : ""}
              >
                <svg
                  width="19"
                  height="19"
                  viewBox="0 0 19 19"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M14.4562 6.94711L16.6326 16.5872L15.1556 18.0643L11.1128 10.2904L7.15429 14.249L8.13949 17.695L7.03169 18.8028L4.44682 14.7409L0.384885 12.156L1.49269 11.0482L4.93942 12.0327L8.89796 8.07411L1.12342 4.03211L2.60049 2.55505L12.2406 4.73151L16.2634 0.708713C16.5572 0.414906 16.9557 0.249847 17.3712 0.249847C17.7867 0.249847 18.1852 0.414906 18.479 0.708713C18.7728 1.00252 18.9378 1.40101 18.9378 1.81651C18.9378 2.23202 18.7728 2.63051 18.479 2.92431L14.4562 6.94711Z"
                    className="fill"
                    fillOpacity="0.6"
                  />
                </svg>
                Flights
              </a>
            </div>
          </div>

          <div className="search_bar search_bar_alltimevisible">
            {router.pathname === "/flights" && <Trip {...{ trip, setTrip }} />}
            {router.pathname === "/" && (
              <div className="box">
                <div className="inputs">
                  <input
                    type="text"
                    className="classic-input location-input"
                    placeholder="LOCATION"
                  />
                  <DatePicker
                    wrapperClassName="classic-datepicker calendar-icon"
                    selectsRange={true}
                    minDate={new Date()}
                    startDate={hotelStartDate}
                    endDate={hotelEndDate}
                    onChange={(update) => {
                      setHotelDateRange(update);
                    }}
                    monthsShown={2}
                  />
                  <select name="" id="" className="classic-select">
                    <option value="">ROOM</option>
                    <option value="">Options</option>
                  </select>
                </div>
                <button className="search_button" onClick={handleSearch}>
                  SEARCH
                </button>
              </div>
            )}
            {router.pathname === "/flights" && (
              <div className="box inputs_flight">
                <div className="inputs">
                  <input
                    type="text"
                    className="classic-input location-input"
                    placeholder="FLYING FROM"
                  />
                  <input
                    type="text"
                    className="classic-input location-input"
                    placeholder="FLYING TO"
                  />
                  {trip === "single" ? (
                    <div>
                      <DatePicker
                        wrapperClassName="classic-datepicker calendar-icon"
                        selected={flightDate}
                        onChange={(date) => setFlightDate(date)}
                        minDate={flightDate}
                        showDisabledMonthNavigation
                        shouldCloseOnSelect={false}
                      />
                    </div>
                  ) : (
                    <div>
                      <DatePicker
                        wrapperClassName="classic-datepicker calendar-icon"
                        selectsRange={true}
                        minDate={new Date()}
                        startDate={flightStartDate}
                        endDate={flightEndDate}
                        onChange={(update) => {
                          setFlightDateRange(update);
                        }}
                        monthsShown={2}
                      />
                    </div>
                  )}
                  <select name="" id="" className="classic-select flight_select">
                    <option value="">ECONOMY - 1 PASSENGER</option>
                    <option value="">Options</option>
                  </select>
                </div>
                <button className="search_button" onClick={handleSearch}>
                  SEARCH
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    </section>
  );
  a;
};

export default Banner;
