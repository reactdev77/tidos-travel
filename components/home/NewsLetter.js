import styles from "./Home.module.css";

export default function NewsLetter() {
  return (
    <section id="newsletter" className={styles.newsletter}>
      <div className="wrapper">
        <div className="row">
          <div className="col-12">
            <div
              className={`col-xs-12 col-sm-7 col-md-7 col-lg-7 ${styles.leftside}`}
            >
              <h1>Subscribe Our Newsletter</h1>
              <p>
                Subscribe to our newsletter to get latest infomation about
                flights and travels
              </p>
              <div className={styles.form}>
                <form>
                  <div className="col-xs-12 col-sm-7 col-md-8 col-lg-8">
                    <input type="text" placeholder="Please enter your email" />
                  </div>
                  <div className="col-xs-12 col-sm-5 col-md-4 col-lg-4">
                    <button>Submit</button>
                  </div>
                </form>
              </div>
              {/* <div className={styles.terms}>
                                <span>{`By clicking Sign Up you're confirming that you agree with our `}</span><span><a>Terms and Conditions</a></span>
                            </div> */}
            </div>

            <div
              className={`col-xs-12 col-sm-5 col-md-5 col-lg-5 ${styles.newsletterimg}`}
            >
              <img src="/images/newsletter.png" alt="img_newsletter" />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
