import styles from "./Home.module.css";

export default function Features() {
  return (
    <section id="best-features" className={styles.features}>
      <div className="wrapper">
        <div className="row">
          <div className="col-12">
            <div className="page-heading">
              <h1>Why book TIDOS</h1>
            </div>
            <div className="col-sm-6 col-md-6" style={{ textAlign: "center" }}>
              <img
                className={styles.coverimg}
                src="/images/why_book.png"
                alt="why_book"
              />
            </div>

            <div className="col-sm-6 col-md-6">
              <div className={styles.rightside}>
                <div className={styles.card}>
                  <div>
                    <img src="/images/icons/feature.png" alt="img_feature" />
                  </div>
                  <div>
                    <p>Best Price Guarantee</p>
                    <p className={styles.content}>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud
                    </p>
                  </div>
                </div>
                <div className={styles.card}>
                  <div>
                    <img src="/images/icons/feature.png" alt="img_feature" />
                  </div>
                  <div>
                    <p>Best Price Guarantee</p>
                    <p className={styles.content}>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud
                    </p>
                  </div>
                </div>
                <div className={styles.card}>
                  <div>
                    <img src="/images/icons/feature.png" alt="img_feature" />
                  </div>
                  <div>
                    <p>Best Price Guarantee</p>
                    <p className={styles.content}>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
