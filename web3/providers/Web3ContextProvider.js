import React, {
  createContext,
  useCallback,
  useContext,
  useMemo,
  useState,
} from "react";
import { StaticJsonRpcProvider, Web3Provider } from "@ethersproject/providers";
import WalletConnectProvider from "@walletconnect/web3-provider";
import Web3Modal from "web3modal";

import { useToast } from "@chakra-ui/react";
import { providerError } from "../helpers/error-helper";
//   import { getUserByWalletAddress, saveUser } from "../database/Store";

export const Web3Context = createContext(null);

export const useWeb3Context = () => {
  const web3Context = useContext(Web3Context);
  if (!web3Context) {
    throw new Error(
      "useWeb3Context() can only be used inside of <Web3ContextProvider />"
    );
  }
  const { onChainProvider } = web3Context;
  return useMemo(() => {
    return { ...onChainProvider };
  }, [onChainProvider]);
};

export default function Web3ContextProvider({ children }) {
  const toast = useToast();
  const [provider, setProvider] = useState(
    new StaticJsonRpcProvider("https://bsc-dataseed.binance.org/")
  );
  const [address, setAddress] = useState("");
  const [chainId, setChainId] = useState();
  const [web3Modal, setWeb3Modal] = useState();
  const [isConnected, setIsConnected] = useState(false);

  const connect = useCallback(async () => {
    const DEFAULT_CHAIN = 56;

    try {
      const providerOptions = {
        walletconnect: {
          package: WalletConnectProvider,
          options: {
            rpc: {
              56: "https://bsc-dataseed.binance.org:443/",
            },
          },
        },
      };

      const web3Modal = new Web3Modal({
        network: "mainnet",
        cacheProvider: true,
        providerOptions,
      });
      const rawProvider = await web3Modal.connect();
      const connectedProvider = new Web3Provider(rawProvider, "any");
      const chainId = await connectedProvider
        .getNetwork()
        .then((network) => Number(network.chainId));

      const connectedAddress = await connectedProvider.getSigner().getAddress();

      // check to see if user exists
      // const user = await getUserByWalletAddress(connectedAddress);
      // if (!user) {
      // if no user, save user walletAddress to database
      //   await saveUser({ walletAddress: connectedAddress });
      // }

      if (chainId !== DEFAULT_CHAIN) {
        try {
          await rawProvider.request({
            method: "wallet_switchEthereumChain",
            params: [{ chainId: "0x38" }],
          });
        } catch (error) {
          if (error.code === 4902) {
            try {
              await rawProvider.request({
                method: "wallet_addEthereumChain",
                params: [
                  {
                    chainId: "0x38",
                    chainName: "Binance Smart Chain",
                    rpcUrls: ["https://bsc-dataseed.binance.org:443/"],
                  },
                ],
              });
            } catch (changeError) {
              const { text } = providerError(changeError);
              toast({
                title: text,
                variant: "solid",
                isClosable: true,
                status: "error",
                position: "bottom-left",
              });
            }
          }
        }
      }

      rawProvider.on("accountsChanged", (accounts) => {
        setAddress(accounts[0]);
      });

      rawProvider.on("chainChanged", (chainId) => {
        setChainId(Number(chainId));
      });

      setAddress(connectedAddress);
      setChainId(chainId);
      setProvider(connectedProvider);
      setIsConnected(true);
      setWeb3Modal(web3Modal);
      localStorage.setItem("isConnected", true);
      toast({
        title: `Wallet Connected!`,
        variant: "solid",
        isClosable: true,
        status: "success",
        position: "bottom-left",
      });

      return connectedProvider;
    } catch (error) {
      if (error.message !== "User closed modal") {
        const { text } = providerError(error);
        toast({
          title: text,
          variant: "solid",
          isClosable: true,
          status: "error",
          position: "bottom-left",
        });
      }
    }
  }, [toast]);

  const disconnect = useCallback(async () => {
    try {
      web3Modal.clearCachedProvider();
      localStorage.removeItem("isConnected");
      toast({
        title: `Wallet Disconnected!`,
        variant: "solid",
        isClosable: true,
        status: "success",
        position: "bottom-left",
      });
      setAddress(null);
      setChainId(null);
      setIsConnected(false);
    } catch (err) {
      console.log(err);
    }
  }, [toast, web3Modal]);

  const onChainProvider = useMemo(
    () => ({
      connect,
      disconnect,
      isConnected,
      address,
      chainId,
      provider,
    }),
    [connect, disconnect, isConnected, address, chainId, provider]
  );

  return (
    <Web3Context.Provider value={{ onChainProvider }}>
      {children}
    </Web3Context.Provider>
  );
}
