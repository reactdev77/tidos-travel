export const providerError = (err) => {
  let text = "Something went wrong";

  if (err.code && err.code === -32603) {
    if (err.data && err.data.message) {
      text = err.data.message.includes(":")
        ? err.data.message.split(":")[1].trim()
        : err.data.data || err.data.message;
    }

    if (
      err.data &&
      err.data.message &&
      err.data.message.includes("gas required exceeds allowance")
    ) {
      text = "Insufficient balance to make a transaction";
    }

    if (
      err.data &&
      err.data.message &&
      err.data.message.includes("Price too low")
    ) {
      text = "Price too low";
    }

    if (
      err.data &&
      err.data.message &&
      err.data.message.includes("Item sold")
    ) {
      text = "Item sold";
    }
  }

  if (err.code && err.code === 4001) {
    if (err.message.includes("User denied transaction signature")) {
      text = "User denied transaction signature";
    }
  }

  return { text, error: err };
};
