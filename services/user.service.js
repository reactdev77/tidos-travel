import { fetchWrapper } from "../helpers/fetch-wrapper";

const baseUrl = "https://tido-payment.herokuapp.com/api";

export const login = (email, password) => {
  return fetchWrapper.post(`${baseUrl}/users/check`, { email, password });
};

export const signup = (user) => {
  return fetchWrapper.post(`${baseUrl}/users`, user);
};
